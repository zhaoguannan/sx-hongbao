<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>三夏送福</title>
  <style type="text/css" >
    html{
      font-size: 100px;/*640px: 1rem=100px*/
    }
    html, body {
      margin: 0;
      padding: 0;
      position: relative;
      height: 100%;
      overflow: hidden;
      background: #F4F4F4;
      z-index: -4;
    }

    .mainBox {
      position: relative;
      margin: 0 auto;
      max-width: 640px;
      height: 100%;
      background: #FFF;
      overflow: hidden;
      z-index: -3;
    }
    .mainBox .bg{
      position: relative;
      display: block;
      overflow: hidden;
      height: 100%;
      background: url("<%=request.getContextPath()%>/jsp/image/red.jpg") no-repeat;
      background-size: cover;
      z-index: -2;
    }
    .container{
      margin: 0 auto;
      position: absolute;
      bottom: .3rem;
      left: 0;
      right: 0;
    }
    .span{
      width: 3.9rem;
      margin: 0 auto;
    }
    .span span{
      display: block;
      text-align: center;
      font-size: .4rem;
      line-height: .82rem;
      color: #c00000;
      font-weight: 400;
      /*background: #fec716;*/
      background: radial-gradient(circle at 100% 0, #f25400, #fec716);
      -webkit-border-radius: .41rem;
      -moz-border-radius: .41rem;
      border-radius: .41rem;
      box-shadow: 1px 2px 4px rgba(172, 4, 0, 0.62);
      text-shadow:  2px 3px 6px rgba(145, 4, 0, 0.4);
      font-family: Arial, Helvetica, 'STHeiti STXihei', 'Microsoft YaHei', Tohoma, sans-serif;
    }
    p{
      padding-top: .2rem;
      font-size: .26rem;
      color: rgba(255,255,255,0.7);
      display: block;
      text-align: center;
      text-shadow: 0px 2px 1px rgba(128, 27, 0, 0.75);
    }
  </style>
  <script>
    ~function () {
      let computed = function () {
        let desW = 640;// 设计稿的宽度
        let devW = document.documentElement.clientWidth; // 设备的宽度
        if (devW >= 640) {
          document.documentElement.style.fontSize = '100px';
          return;
        }
        document.documentElement.style.fontSize = devW / desW * 100 + 'px';
      }
      computed();
      window.addEventListener('resize', computed, false);
    }();
  </script>
</head>
<body>
  <main class="mainBox">
    <section class="bg">
      <div class="container">
        <div class="span">
          <span>${code }</span>
        </div>
        <p>*${msg }</p>
      </div>
    </section>
  </main>
</body>
</html>