<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>${cultureVideoInfo.videoName }</title>

  <script src="<%=request.getContextPath()%>/api/public/js/zepto.min.js"></script>
  <script src="http://res2.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
    <script>
        (function(){
            function w () {
                var r = document.documentElement;
                var a = r.getBoundingClientRect().width;
                if (a > 750) {
                    a = 750;
                };
                rem = a / 7.5;
                r.style.fontSize = rem + 'px';
            };
            var t;
            w();
            window.addElementListener('resize',function(){
                clearTimeout(t);
                t = setTimeout(w,300)
            },false);
        })()
    </script>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        ul,li{
            list-style: none;
        }
        .container{
        }
        header{
            height: 3.94rem;
            background: white;
        }
        main{
            /*height: 16rem;*/
            background: white;
        }
        main .nav1{
            /*margin: 0 0 0.3rem 0;*/
            margin-top: 0.2rem;
            height: 2.72rem;
            width: 6.9rem;
            color: #5E5E5E;
            padding: 0.3rem 0.2rem 0.3rem 0.35rem;
        }
        .fxc{
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
        }
        .fxnl{
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            justify-content: space-around;
        }
        .fxnr{
            display: flex;
            flex-direction: column;
            align-items:flex-end;
            justify-content: flex-end;
        }
        .fenxiang{
            height: 2.42rem;
            width: 7.32rem;
            margin: 0.09rem;
            border: 0.01rem dashed grey;
        }
        .fencontent{
            height: 2rem;
            width: 6.9em;
            margin: 0.14rem;
            background: url("9.png") no-repeat;
            background-size: cover;
        }
        .fcl{
            font-size: .26rem;
            height: 2rem;
            width: 4.3rem;
            padding-left: 0.3rem;
            padding-bottom: 0.41rem;
        }
        .fcl span{
            margin: .45rem 0 0 .1rem;
        }
        .fcr{
            font-size: .37rem;
            height: 2rem;
            width: 2.6rem;
        }
        .fcr .bt{
            border: none;
            border-radius: 1.46rem;
            background: linear-gradient(to right,#FEC015,#FA654A);
            color: #FFFFFF;
            font-size: 0.28rem;
            width: 2rem;
            height: 0.6rem;
            margin-right: 0.43rem;
            margin-bottom: 0.31rem;
        }
        .fcr span{
            margin-right: 0.43rem;
            margin-bottom: 0.14rem;
        }
        .nav1-bottom{
            height: 0.2rem;
            background: whitesmoke;
        }
        main .nav1 .nav1_left{
            float: left;
            width: 2.33rem;
            height: 2.72rem;
        }
        main .nav1 .nav1_right{
            float: left;
            border: 1px solid #FB876E;
            border-left: none;
            /*margin: 0.32rem 0.2rem 0.28rem 0;*/
            height: 2.68rem;
            width: 4.5rem;
        }
        main .nav1 .nav1_right h1{
            font-size: 0.35rem;
            margin: 0.3rem 0 0.15rem 0.35rem;
        }
        main .nav1 .nav1_right p{
            float:left;
            line-height: 0.35rem;
            margin: 0rem 0rem 0.08rem 0.05rem;
        }
        main .nav1 .nav1_right p span{
            color: #5E5E5E;
            font-size: 0.2rem;
            float: left;
            margin: 0rem 0rem 0rem 0.15rem;
        }
        main .nav2{

            margin: 0.3rem;
        }
        .nav2-bottom{
            height: 0.2rem;
            background: whitesmoke;
        }
        main .nav2 li{
            color: #5E5E5E;
            margin-top: 0.47rem;
            margin-left: 0.3rem;
            margin-bottom: 0.3rem;
            padding-left: 0.3rem;
            font-size: 0.3rem;
            font-weight: bold;
            border-left: 6px solid orange;
            list-style-type: none;
        }
        main .nav2 p{
            text-indent:2em;
            color: #999999;
            word-wrap:break-word;
            margin-right: 0.1rem;
            margin-bottom: 0.05rem;
            padding-left: 0.2rem;
            font-size: 0.28rem;
            line-height: 0.35rem;
        }
        main .nav1 span{
            padding-right: 0.3rem;
        }
        main .nav3 ul{
            padding: .3rem;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            flex-wrap: wrap;
        }
        .nav3 ul:after{
            content: "";
            width:30%;
        }
        .nav3 h1{
            font-size: 0.3rem;
            color: #5E5E5E;
            padding-left: 0.3rem;
            border-left: 6px solid orange;
            margin: 0.6rem 0.1rem 0 0.1rem;
        }
        .nav3 li{
            text-align: center;
            font-size: 0.3rem;
            color: #5E5E5E;
            list-style-type: none;
            padding-bottom: .3rem;
        }
        .nav3 h3{
            font-weight: bold;
            margin-top: 0.1rem;
            color: #5E5E5E;
            margin-bottom: 0.1rem;
        }
        .nav3 p{
            color: #828282;
            font-weight: normal;
        }
        #zhezhao {
            height:100%;
            background-color:#000;
            filter:alpha(opacity=50);
            -moz-opacity:0.5;
            opacity:0.8;
            position:fixed;
            left:0;
            top:0;
            display:none;
            z-index:1000;
        }
        #zhezhao button{
            position: relative;
            top: 9rem;
            left: 1.5rem;
            color: #FA654A;
            font-size: 15px;
            background: none;
            width: 4.5rem;
            height: 0.8rem;
            border: 2px solid #FA654A;
            border-radius: 19px;
        }
        #zhezhao span{
            color: #FA654A;
            height: 0.45rem;
            width: 5rem;
            font-weight: bolder;
            display: block;
            font-size: 21px;
            position: relative;
            top: 5.45rem;
            left: 2rem;
        }
        #toshare{
            width: 0.57rem;
            height: 0.57rem;
            border-radius: 50%;
            background: #000000;
            color: white;
            font-size: 0.15rem;
            text-align: center;
            position: relative;
            top: 0.6rem;
            left: 6.5rem;
        }
        #zhezhao p{
            width: 0.57rem;
            height: 0.57rem;
            color: white;
            font-size: 0.6rem;
            position: relative;
            top: 1rem;
            left: 0.5rem;
        }

        .frcfs{
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: flex-start;
        }
        .frfsfs{
            display: flex;
            flex-direction: row;
            align-items: flex-start;
            justify-content: flex-start;
        }
        .lineClamp{
            white-space:nowrap;
            overflow:hidden;
            text-overflow:ellipsis;
        }
        .frrcfs{
            display: flex;
            flex-direction: row;
            align-items: center;
            flex-wrap: wrap;
            justify-content: flex-start;
        }
        .movie_container {
            height: 3rem;
        }
        .movie_container img {
            width: 2.53rem;
            height: 3rem;
            border-bottom-left-radius: .1rem;
            border-top-left-radius: .1rem;
        }
        .movie_container .right {
            flex:  1;
            display: flex;
            flex-direction: column;
            height: 3rem;
            border: solid 1px #FB876E;
            border-bottom-right-radius: .1rem;
            border-top-right-radius: .1rem;
            padding-left: .24rem;
        }
        .movie_container .right dl{
            padding-top: .24rem;
            flex: 1;
        }
        .movie_container .right dl dt{
            height: .46rem;
            line-height: .46rem;
            font-size: .33rem;
            padding-bottom: .08rem;
            color: #333333
        }
        .movie_container .right dl dd span{
            height: .34rem;
            font-size: .25rem;
            line-height: .34rem;
            color: #C8C8C8;
        }
        .movie_container .right .bot{
            padding-bottom: .24rem;
        }
        .movie_container .right .bot .actor li,  .movie_container .right .bot .actor span{
            height: .4rem;
            font-size: .28rem;
            line-height: .4rem;
            color: #999;
        }
    </style>
</head>
<body style="width: 7.5rem; overflow-x: hidden;">
<div class="container">
    <header style="width: 7.5rem;">
        <video style="width: 100%;"  src="${trailerVideopath }" controls="controls"  poster="${publishOutimage }" width="100%" height="100%">
        </video>
    </header>
    <main style="width: 7.5rem;">

        <div style="padding: .3rem;">
            <div class="frcfs movie_container">
                <img src="${publishOutimage }" alt="">
                <div class="right" style="border-left: none;">
                    <dl>
                        <dt>${cultureVideoInfo.videoName }</dt>
                        <dd class="frcfs">
                            <c:forEach items="${yingshileibie}" var="item" varStatus="status">
                                <span>${item}&nbsp;</span>
                            </c:forEach>
                        </dd>
                    </dl>
                    <div class="bot">
                        <div class="actor frcfs">
                            <span>导演：</span>
                            <ul class="frcfs">
                                <c:forEach items="${daoyan}" var="item" varStatus="status">
                                    <li>${item}&nbsp;</li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="actor frcfs">
                            <span>主演：</span>
                            <ul class="frcfs" style=" flex: 1; overflow: hidden; height: .8rem;">
                                <c:forEach items="${yanyuan}" var="item" varStatus="status">
                                    <li>${item}&nbsp;</li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="fenxiang">
            <div class="fencontent fxc">
                <div class="fcl fxnl">
                    <div class="fxc"><span style="font-size: .26rem; color: #333333;">立即分享得红包</span><span style="color: #FA654A; font-size: 0.42rem;font-weight: bold;">${everydayHongbao }</span>
                        <span style="font-size: 0.26rem; color: #FA654A;">元</span>
                    </div>
                    <div><span style="font-size: .3rem;font-weight: bold; color: #FA654A;">连续分享还能拿大红包</span></div>
                </div>
                <div class="fcr fxnr">
                    <button class="bt" onclick="an()">立享红包</button>
                    <div class="fxc"><span onclick="move()" style="color: #FA654A; font-size: 0.2rem;">活动规则<img  src="<%=request.getContextPath() %>/api/image/wenhao.png" style="width: 0.2rem; height: 0.22rem;"></span></div>
                </div>
            </div>
        </div>

        <div class="nav1-bottom"></div>
        <div class="nav2">
            <ul>
                <li>剧情简介</li>
                <p>${cultureVideoInfo.videoDescribe4}</p>
                <li>项目阐述</li>
                <p>${cultureVideoInfo.videoDescribe1}</p>
                <li>关于剧本</li>
                <p>${cultureVideoInfo.videoDescribe5}</p>
                <li>人物小传</li>
                <p>${cultureVideoInfo.videoDescribe6}</p>
            </ul>
        </div>
        <div class="nav2-bottom"></div>
        <div class="nav3">
            <h1>演职人员</h1>
            <ul>
                <c:forEach items="${listcultureActors}" var="item" varStatus="status">
                <li><img src="${item.actorsImage}" alt="" style="width: 2rem; height: 2.6rem"><h3>${item.actorsName}</h3><p>${item.reserveFields1}</p></li>
                </c:forEach>
            </ul>
        </div>
    </main>
</div>

  <div class="mainBox">
  <input id="openid" type="hidden" value="${openid }">
  <input id="mchid" type="hidden" value="${mchid }">
      <input id="busId" type="hidden" value="${busId }">
      <input id="from" type="hidden" value="${from }">
      <input id="tile" type="hidden" value="${cultureVideoInfo.videoName }">
      <input id="imgurl" type="hidden" value="${publishOutimage }">
      <input id="userId" type="hidden" value="${userId }">
      <input id="videoDescribe4" type="hidden" value="${cultureVideoInfo.videoDescribe4 }">
  </div>
<%--  <div>
      <a href="#" onclick="an()">授权登录${openid} </a>
  </div>--%>
   <%--  <div class="textImg">
        <ul>
          <li id="orderVip" >${code }</li>
        </ul>
       <img src="<%=request.getContextPath()%>api/image/ewm.png" alt="">
      </div>
    </div>
  </div>--%>

    <script>

        function move() {
            window.location.href="http://share.shijiebox.com/api/culture/share/share1?_ijt=dmgcevk3c0icksn66o8ldgohef";
        }

        function an(){
            var mchid=$("#mchid").val();
            var openid=$("#openid").val();
            var busId=$("#busId").val();
            window.location.href='http://share.shijiebox.com/api/index/SQ?mchid='+mchid+'&action=RCJ'+'&busId='+busId;
            // $.ajax({
            //     url: 'http://localhost:8099/SQ?mchid='+mchid+'&action=RCJ'+"&busId="+busId,
            //     type: 'get',
            //     dataType: 'json',
            //     contentType: "application/json",
            //     data: {},
            //     success: function (res) {
            //
            //     }
            // });
        }

        /*
         * 注意：
         * 1. 所有的JS接口只能在公众号绑定的域名下调用，公众号开发者需要先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
         * 2. 如果发现在 Android 不能分享自定义内容，请到官网下载最新的包覆盖安装，Android 自定义分享接口需升级至 6.0.2.58 版本及以上。
         * 3. 常见问题及完整 JS-SDK 文档地址：http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
         *
         * 开发中遇到问题详见文档“附录5-常见错误及解决办法”解决，如仍未能解决可通过以下渠道反馈：
         * 邮箱地址：weixin-open@qq.com
         * 邮件主题：【微信JS-SDK反馈】具体问题
         * 邮件内容说明：用简明的语言描述问题所在，并交代清楚遇到该问题的场景，可附上截屏图片，微信团队会尽快处理你的反馈。
         */
         var appId="";
         var  timestamp="";
         var nonceStr="";
         var signature="";
         $(function(){
        	 var mchid=$("#mchid").val();
        	 var openid=$("#openid").val();
             var busId=$("#busId").val();
             var from=$("#from").val();
             var tile=$("#tile").val();
             var imgurl=$("#imgurl").val();
             var userId=$("#userId").val();
             var videoDescribe4=$("#videoDescribe4").val();
             $.ajax({
                 type: 'POST',
                 url: 'http://share.shijiebox.com/api/share/clickShareUrl',
                 contentType: 'application/json',//规定连同请求发送到服务器的数据；
                 // data:{
                 //   userId: 2,
                 //   shareLogId: 6
                 // },    //映射或字符串值，规定连同请求发送到服务器的数据；
                 data:JSON.stringify({
                     "userId": userId,
                     "shareLogId": busId
                 }),
                 success: function (data) {
                     console.log(data)
                 },      //请求成功时执行的回调函数；
             });
        	 $.ajax({
                 url: 'http://share.shijiebox.com/api/culture/share/share?mchid='+mchid+'&openid='+openid+'&busId='+busId+"&from="+from,
                 type: 'get',
                 dataType: 'json',
                 contentType: "application/json",
                 data: {},
                 success: function (res) {

                	 wx.config({
                	     debug: false,
                	     appId: res.appId,
                	     timestamp:res.timestamp ,
                	     nonceStr: res.noncestr,
                	     signature: res.signature,
                	     jsApiList: [
                	       'onMenuShareTimeline',
                	       'onMenuShareAppMessage'
                	     ]
                	 });
                	 wx.ready(function () {

                	     // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口
                	         wx.onMenuShareTimeline({
                	             title: tile,
                	             link: 'http://share.shijiebox.com/api/culture/share/query?mchid='+mchid+'&openid='+openid+'&busId='+busId,
                	             imgUrl: imgurl,
                	             success: function () {
                                     $.ajax({
                                         type: 'POST',
                                         url: 'http://share.shijiebox.com/api/share/userShareSuccess',
                                         contentType: 'application/json',//规定连同请求发送到服务器的数据；
                                         // data:{
                                         //   userId: 2,
                                         //   shareLogId: 6
                                         // },    //映射或字符串值，规定连同请求发送到服务器的数据；
                                         data:JSON.stringify({
                                             "userId": userId,
                                             "shareLogId": busId,
                                             "shareRest": '0',
                                             "shareType": 'wx',
                                             "shareUrl": 'http://share.shijiebox.com/api/culture/share/query?mchid='+mchid+'&openid='+openid+'&busId='+busId
                                         }),
                                         success: function (data) {
                                             console.log(data)
                                         },      //请求成功时执行的回调函数；
                                     })

                                     // {
                                     //     "shareLogId": "string",
                                     //     "shareRest": "string",
                                     //     "shareType": "string",
                                     //     "shareUrl": "string",
                                     //     "userId": "string"
                                     // }
                	             }
                	         });
                	         //alert('已注册获取“分享到朋友圈”状态事件');
                	      wx.onMenuShareAppMessage({
                	             title: tile,
                	             desc: videoDescribe4.substring(0,30),
                	             link: 'http://share.shijiebox.com/api/culture/share/query?mchid='+mchid+'&openid='+openid+'&busId='+busId,
                	             imgUrl: imgurl,

                	             success: function () {

                                     $.ajax({
                                         type: 'POST',
                                         url: 'http://share.shijiebox.com/api/share/userShareSuccess',
                                         contentType: 'application/json',//规定连同请求发送到服务器的数据；
                                         // data:{
                                         //   userId: 2,
                                         //   shareLogId: 6
                                         // },    //映射或字符串值，规定连同请求发送到服务器的数据；
                                         data:JSON.stringify({
                                             "userId": userId,
                                             "shareLogId": busId,
                                             "shareRest": '0',
                                             "shareType": 'wx',
                                             "shareUrl": 'http://share.shijiebox.com/api/culture/share/query?mchid='+mchid+'&openid='+openid+'&busId='+busId
                                         }),
                                         success: function (data) {
                                             console.log(data)
                                         },      //请求成功时执行的回调函数；
                                     })


                                 }

                	         });

                	 });
                 }
               });




         })
    </script>



</body>
</html>

