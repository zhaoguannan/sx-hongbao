<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>视小宝</title>
<style>
     html{
      font-size: 100px;/*640px: 1rem=100px*/
    }
    html, body {
      position: relative;
      height: 100%;
      overflow: hidden;
      background: #F4F4F4;
      z-index: -4;
      -webkit-user-select: none;
      user-select: none;
      -webkit-touch-callout: none;
    }
    body,ul,li{
      margin: 0;
      padding: 0
    }

    body {
      font-size: 12px;
      font-style: normal;
      font-family: "\5FAE\8F6F\96C5\9ED1",Helvetica,sans-serif
    }

    ul,ol {
      list-style: none
    }

    a {
      text-decoration: none;
      background-color: transparent
    }

    img {
      border-style: none
    }

    img:not([src]) {
      display: none
    }

    html {
      -webkit-touch-callout: none;
      -webkit-text-size-adjust: 100%
    }

    .mainBox {
      position: relative;
      margin: 0 auto;
      max-width: 6.4rem;
      height: 100%;
      background: #FFF;
      overflow: hidden;
      z-index: -3;
    }
    .default {
      /*background: url("first/first.png") no-repeat;*/
      background-size: 100%;
      z-index: -2;
      width: 100%;
      height: 100%;
    }

    .textImg {
      position: absolute;
      left: 0;
      right: 0;
      bottom: .2rem;
    }
    .textImg img{
      display: block;
      width: 1.5rem;
      margin: 0 auto;
      animation: sport .6s linear;
      transform: translateY(0);
    }
    ul {
      /*top: 64%;*/
      display: flex;
      -webkit-box-orient: horizontal;
      -webkit-box-direction: normal;
      -ms-flex-direction: row;
      flex-direction: row;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -ms-flex-pack: distribute;
      justify-content: center;
      padding-bottom: .15rem;
    }
    ul li {
      font-size: .22rem;
      color: #fce64e;
      background-image:-webkit-linear-gradient(bottom, #fce64e, #e7a505, #fce64e);
      -webkit-background-clip:text;
      -webkit-text-fill-color:transparent;
    }
    .animate2 {
      animation-name: floating;
      animation-delay: 1600ms;
      animation-iteration-count: infinite;
      animation-timing-function: ease-in-out;
      animation-duration: 3s;
    }
    .animate {
      transform: translate3d(0,-50%,0);
      transition: transform 1000ms cubic-bezier(.52,.01,.16,1) 1000ms;
    }
    @keyframes floating {
      0%,to {
        transform: translate(0%,-50%)
      }

      25% {
        transform: translate(5px,calc(-50% + 5px))
      }

      50% {
        transform: translate(10px,calc(-50% + 5px))
      }

      75% {
        transform: translate(0%,calc(-50% + 5px))
      }
    }

    @keyframes sport {
      0%   {transform: translateY(100%);opacity: 0;}
      50% {transform: translateY(50%);opacity: 0.5}
      100% {transform: translateY(0);opacity: 1}
    }
    .bigimg {
      position: fixed;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      z-index: -5;
      width: 100%;
      height: 100%;
      margin: 0 auto;
    }
    .logo {
      position: absolute;
      left: .18rem;
      top: .18rem;
    }
    .logo img {
      display: block;
      z-index: 2;
      width: .36rem;
    }
    .text {
      position: absolute;
      top: 46%;
      left: 12%;
      width: 76%;
      margin:  0 auto;
    }
    .text img {
      width: 100%;
      display: block;
      margin:  0 auto;
    }
    .date {
      padding-top: .85rem;
      display: flex;
      align-content: center;
      flex-direction: row;
      justify-content: center;
    }
    .date img {
      width: 2.3rem;
      height: 1.1rem;
      margin:  0 auto;
    }
  </style>
  <script src="<%=request.getContextPath()%>/jsp/static/public/js/zepto.min.js"></script>
  <script src="http://res2.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
  <script>
    ~function ($) {
      let computed = function () {
        //=>DES-W:设计稿宽度 DEV-W:设备的宽度
        let desW = 360;
        let devW = document.documentElement.clientWidth;
        if (devW >= 640) {
          document.documentElement.style.fontSize = '100px';
          return;
        }
        document.documentElement.style.fontSize = devW / desW * 100 + 'px';
      };
      computed();
      window.addEventListener('resize', computed, false);
    }(Zepto);
  </script>
</head>
<body>
  <div class="mainBox">
  <input id="openid" type="text" value="${openid }">
  <input id="mchid" type="text" value="${mchid }">
  <img class="bigimg" src="<%=request.getContextPath()%>/jsp/image/first.jpg" alt="">
    <div class="default">
     <div class="logo">
        <img src="<%=request.getContextPath()%>/jsp/image/logo.png" alt="">
      </div>
      <div class="date">
        <img src="<%=request.getContextPath()%>/jsp/image/date.png" alt="">
      </div>
      <div class="text">
        <img class="animate2 animate" src="<%=request.getContextPath()%>/jsp/image/text.png" alt="">
      </div>
      <div class="textImg">
        <ul>
          <li id="orderVip" >${code }</li>
        </ul>
        <img src="<%=request.getContextPath()%>/jsp/image/ewm.png" alt="">
      </div>
    </div>
  </div>

    <script>
        /*
         * 注意：
         * 1. 所有的JS接口只能在公众号绑定的域名下调用，公众号开发者需要先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
         * 2. 如果发现在 Android 不能分享自定义内容，请到官网下载最新的包覆盖安装，Android 自定义分享接口需升级至 6.0.2.58 版本及以上。
         * 3. 常见问题及完整 JS-SDK 文档地址：http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
         *
         * 开发中遇到问题详见文档“附录5-常见错误及解决办法”解决，如仍未能解决可通过以下渠道反馈：
         * 邮箱地址：weixin-open@qq.com
         * 邮件主题：【微信JS-SDK反馈】具体问题
         * 邮件内容说明：用简明的语言描述问题所在，并交代清楚遇到该问题的场景，可附上截屏图片，微信团队会尽快处理你的反馈。
         */
         var appId="";
         var  timestamp="";
         var nonceStr="";
         var signature="";
         $(function(){
        	 var mchid=$("#mchid").val();
        	 var openid=$("#openid").val();
        	 $.ajax({
                 url: 'http://share.shijiebox.com/sx-hongbao/tickets/share?mchid='+mchid+'&openid='+openid,
                 type: 'get',
                 dataType: 'json',
                 contentType: "application/json",
                 data: {},
                 success: function (res) {
                	
                	 wx.config({
                	     debug: false,
                	     appId: res.appId,
                	     timestamp:res.timestamp ,
                	     nonceStr: res.noncestr,
                	     signature: res.signature,
                	     jsApiList: [
                	       'onMenuShareTimeline',
                	       'onMenuShareAppMessage'
                	     ]
                	 });
                	 wx.ready(function () {

                	     // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口
                	         wx.onMenuShareTimeline({
                	             title: '6月25相约泰山，视小宝高峰论坛，我是您的视小宝，不见不散',
                	             link: 'http://share.shijiebox.com/sx-hongbao/tickets/query?mchid=&openid=',
                	             imgUrl: 'http://share.shijiebox.com/sx-hongbao//jsp/image/ewm.png',
                	             success: function () {

                	             }
                	         });
                	         //alert('已注册获取“分享到朋友圈”状态事件');
                	      wx.onMenuShareAppMessage({
                	             title: '视小宝高峰论坛',
                	             desc: '6月25相约泰山，视小宝高峰论坛，我是您的视小宝，不见不散',
                	             link: 'http://share.shijiebox.com/#/home?mchid=&openid=',
                	             imgUrl: 'http://share.shijiebox.com/sx-hongbao//jsp/image/ewm.png',
                	             
                	             success: function () {
                	                
                	             }
                	           
                	         });

                	 });
                 }
               });
        	 
        	
        	 

         })
    </script>
  


</body>
</html>

