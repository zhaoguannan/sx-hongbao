<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>三夏送福</title>
  <style type="text/css" >
   html{
      font-size: 100px;/*640px: 1rem=100px*/
    }
    html, body {
      margin: 0;
      padding: 0;
      position: relative;
      height: 100%;
      overflow: hidden;
      background: #F4F4F4;
      z-index: -4;
    }
    input{outline:none;}
    .mainBox {
      position: relative;
      margin: 0 auto;
      max-width: 640px;
      height: 100%;
      background: #FFF;
      overflow: hidden;
      z-index: -3;
    }
    .mainBox .bg{
      position: relative;
      display: block;
      overflow: hidden;
      height: 100%;
      background: url("<%=request.getContextPath()%>/jsp/image/red2.png") no-repeat;
      background-size: cover;
      z-index: -2;
    }
    .container{
      padding-top:98%;
    }
    .input{
      display: block;
      margin: auto;
      width: 5rem;
    }
    .input input{
      display: block;
      padding: 0;
      margin: auto;
      background: #b61208;
      border: none;
      height: .9rem;
      border-radius: .9rem;
      width: 100%;
      font-size: .42rem;
      line-height: 1rem;
      text-align: center;
      color: #fcd683;
      font-family: Arial, Helvetica, 'STHeiti STXihei', 'Microsoft YaHei', Tohoma, sans-serif;
      -moz-box-shadow:  inset 0px 0px 6px 3px rgba(100, 8, 0, 0.2);
      -webkit-box-shadow: inset 0px 0px 6px 3px rgba(100, 8, 0, 0.2);
      box-shadow: inset 0px 0px 6px 3px rgba(100, 8, 0, 0.2);
    }
    #command_word::-webkit-input-placeholder{
      color: #fff;
      font-size: .34rem;
      line-height: .9rem;
    }
    #command_word::-moz-placeholder { /* Firefox 19+ */
      color: #fff;
      font-size: .34rem;
      line-height: 1rem;
    }
    #command_word:-ms-input-placeholder { /* IE 10+ */
      color: #fff;
      font-size: .34rem;
      line-height: 1rem;
    }
    #command_word:-moz-placeholder { /* Firefox 18- */
      color: #fff;
      font-size: .34rem;
      line-height: 1rem;
    }
    .input span{
      font-size: .26rem;
      display: block;
      padding-top: .06rem;
      color: #fec716;
      /*text-shadow: 0px 2px 1px rgba(128, 27, 0, 0.75);*/
    }
    .input p{
      margin: 0;
      padding: 0;
      text-align: center;
      display: block;
      font-size: .64rem;
      color: #fec716;
      text-shadow:  1px 4px 4px rgba(172, 4, 0, 1);
    }
    .input strong{
      display: block;
      padding-top: .2rem;
      padding-bottom: .8rem;
      text-align: center;
      font-size: .8rem;
      color: #fec716;
      text-shadow:   1px 4px 4px rgba(172, 4, 0, 1);
    }
    .button{
      margin: auto;
      width: 3.86rem;
      /*margin-left: -1.93rem;*/
    }
    button{
      margin-top: .6rem;
      padding: 0;
      border: 1px solid transparent;
      outline: none;
      width: 3.86rem;
      height: .82rem;
      line-height: .82rem;
      /*background: #fec716;*/
      background: radial-gradient(circle at 100% 0, #f25b13, #fec716);
      color: #c00000;
      font-size: .38rem;
      /*font-weight: bold;*/
      border-radius: .4rem;
      font-family: Arial, Helvetica, 'STHeiti STXihei', 'Microsoft YaHei', Tohoma, sans-serif;
      box-shadow: 1px 3px 4px rgba(172, 4, 0, 1);
      /*text-shadow:  1px 2px 3px rgba(145, 4, 0, 0.4);*/
      text-shadow:  1px 2px 3px rgba(145, 4, 0, 0.4);
    }
    button:active{
      background: radial-gradient(circle at 100% 0, #f24f12, #feb515);
    }

    /*loading*/
    .loadingBac{
      display: none;
      position: absolute;
      z-index: 99;
      height: 100%;
      width: 100%;
      background: rgba(251,49,54,0.4);
    }
    .spinner {
      position: absolute;
      z-index: 999;
      width: 50px;
      height: 50px;
      text-align: center;
      font-size: 10px;
      left: 50%;
      top: 50%;
      margin-left: -25px;
      margin-top: -25px;
    }

    .double-bounce1, .double-bounce2 {
      width: 100%;
      height: 100%;
      border-radius: 50%;
      background-color: #fff;
      opacity: 0.6;
      position: absolute;
      top: 0;
      left: 0;

      -webkit-animation: bounce 2.0s infinite ease-in-out;
      animation: bounce 2.0s infinite ease-in-out;
    }

    .double-bounce2 {
      -webkit-animation-delay: -1.0s;
      animation-delay: -1.0s;
    }

    @-webkit-keyframes bounce {
      0%, 100% { -webkit-transform: scale(0.0) }
      50% { -webkit-transform: scale(1.0) }
    }

    @keyframes bounce {
      0%, 100% {
        transform: scale(0.0);
        -webkit-transform: scale(0.0);
      } 50% {
          transform: scale(1.0);
          -webkit-transform: scale(1.0);
        }
    }
  </style>
  <script src="<%=request.getContextPath()%>/jsp/static/plugins/jquery/jquery.min.js"></script>
  <script src="<%=request.getContextPath()%>/jsp/static/plugins/jquery/md5-utf-8.js"></script>
  <script src="<%=request.getContextPath()%>/jsp/static/plugins/jquery/jquery.cookie.js"></script>
  <script>
    ~function () {
      let computed = function () {
        let desW = 640;// 设计稿的宽度
        let devW = document.documentElement.clientWidth; // 设备的宽度
        if (devW >= 640) {
          document.documentElement.style.fontSize = '100px';
          return;
        }
        document.documentElement.style.fontSize = devW / desW * 100 + 'px';
      }
      computed();
      window.addEventListener('resize', computed, false);
    }();
    $(function() {
    	  //微信内置浏览器浏览H5页面弹出的键盘遮盖文本框的解决办法
    	  window.addEventListener("resize", function () {
    	    if (document.activeElement.tagName == "INPUT" || document.activeElement.tagName == "TEXTAREA") {
    	      window.setTimeout(function () {
    	        document.activeElement.scrollIntoViewIfNeeded();
    	      }, 0);
    	    }
    	  })
    	})
    	$(document).ready(function(){
    /*$("input").focus(function(){
      $(this).addClass("bor");
    });*/
    $("input").blur(function(){
      window.scrollTo(0,document.documentElement.clientHeight)
    });
  });
// hex_md5("123456");
  </script>
  <script type="text/javascript">
  function an(){
	  $("#abc").css("display","block")
  	   var formData = {
  			  openid: $('#openid').val(),
  			  mchid: $('#mchid').val(),
              command_word: md5($('#command_word').val()+'sanxiam'+$('#openid').val())
          };
  	// console.log(3)
          $.ajax({
              type: 'POST',
              url: '<%=request.getContextPath()%>/hb/command_hongbao',
              contentType: "application/json; charset=utf-8",
              data: JSON.stringify(formData),
              success: function (data) {
            	  $("#abc").css("display","none")
              	$('#code').html(data.code);
              	$('#msg').html(data.msg);
              },
              error: function () {
            	  $("#abc").css("display","none")
              }
          }); 
        //  console.log(2)
  }
  </script>
</head>
<body>
<input type="hidden" id="openid" value="${openid }">
<input type="hidden" id="mchid" value="${mchid }">
  <main class="mainBox">
  <div class="loadingBac" id="abc">
  <div class="spinner" >
         <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
  </div>
</div>
    <section class="bg">
      <div class="container">
        <div class="input">
         <!--  <p>输入密令</p>
          <strong>领取专属红包</strong> -->
          <input type="text" id="command_word" placeholder="请输入您的密令">
          <span id="code"></span>
          <span id="msg"></span>
        </div>
        <div class="button" onclick="an()">
          <button >即刻领取</button>
        </div>
      </div>
    </section>
  </main>
</body>
</html>