package com.sanxia.common.enums;

public enum ActionEnum {
    RSHb("RSHb","领取红包");

    private String code;
    public String msg;

    ActionEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

   
    
}