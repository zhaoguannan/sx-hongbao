package com.sanxia.market.Controller;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sanxia.common.enums.CodesEnum;
import com.sanxia.common.utils.JSONUtils;
import com.sanxia.common.utils.JsonUtil;
import com.sanxia.common.utils.StringUtil;
import com.sanxia.market.service.hongbao.HongbaoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author zgn 框架测试controller类
 *
 */

@Controller
@RequestMapping("/hb")
@Api("红包相关api")
public class HongbaoController {
	
	private final Logger logger = (Logger) LoggerFactory.getLogger(HongbaoController.class);
	
  
    
    @Autowired
    private HongbaoService hongbaoService;
    
	
	@ApiOperation("领红包接口")
	@RequestMapping(value="/hongbao",method=RequestMethod.GET)
	@ResponseBody
	public Map receive(@RequestParam("mchid")  String mchid,@RequestParam("openid") String openid
				) throws IOException {
		logger.info("方法：hongbao（）入参:mchid="+mchid+"; openid="+openid);
			String code = "领取失败";
			String msg="哎呀，红包滑走了，请稍后再试。";
			Map<String,String> map=hongbaoService.sendhongbao(mchid,openid);
			if(CodesEnum.SUCCESS.getCode().equals(map.get("code"))) {
				code="领取成功";
				msg="红包已奉上，回到公众号中收获惊喜吧。 ";
			}
			if(CodesEnum.FAIL0007.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg="您已领过啦，不要贪心哦。 ";
			}
			if(CodesEnum.FAIL0102.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg="哎呀，您来晚啦，活动已结束。  ";
			}
			if(CodesEnum.FAIL0103.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg="哎呀，您来早啦，活动尚未开始。  ";
			}
			if(CodesEnum.FAIL0004.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg=map.get("msg");
			}
			Map<String,String> maprest = new HashMap();
			map.put("code", code);
			map.put("msg", msg);
			return map;
	}
	
	@ApiOperation("是否已经发过红包,是否已经交卷,红包金额,等参数")
	@RequestMapping(value="/userInfo",method=RequestMethod.GET)
	@ResponseBody
	public Map<String,String> userInfo(@RequestParam("mchid")  String mchid,@RequestParam("openid") String openid
			,Map<String, Object> maprest	) throws IOException {
		    logger.info("方法：userInfo（）入参:mchid="+mchid+"; openid="+openid);
			Map<String,String> map=hongbaoService.userInfo(mchid,openid);
			return map;
	}
	
	@ApiOperation("交卷接口")
	@RequestMapping(value="/userExamSubmit",method=RequestMethod.GET)
	@ResponseBody
	public Map<String,String> userExamSubmit(@RequestParam("openid") String openid,@RequestParam("score") Integer score
			,@RequestParam("name") String name,@RequestParam("mobile") String mobile,@RequestParam("address") String address
				) throws IOException {
		logger.info("方法：userExamSubmit（）入参: openid="+openid);
			String code = "已领红包,已交卷";
			String msg="您已领过啦，不要贪心哦。 ";
			Map<String,String> map=hongbaoService.userExamSubmit(openid,score,name,mobile,address);
			return map;
	}
	
	@ApiOperation("口令红包页面")
	@RequestMapping(value="/command_hongbao_ui",method=RequestMethod.GET)
	public String command_hongbao_ui(@RequestParam("mchid")  String mchid,@RequestParam("openid") String openid
			,Map<String, Object> maprest	)  {
		maprest.put("mchid", mchid);
		maprest.put("openid", openid);
		return "command_hongbao_ui";
	}
	

	
	@ApiOperation("口令红包")
	@RequestMapping(value="/command_hongbao",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> command_hongbao(
			@RequestBody Map<String,Object> pammermap) throws IOException {
		String mchid=(String) pammermap.get("mchid");
		String openid=(String) pammermap.get("openid");
		String command_word=(String) pammermap.get("command_word");
		logger.info("方法：command_hongbao（）入参:mchid="+mchid+"; openid="+openid+"; command_word="+command_word);
		Map<String, Object> maprest =new HashMap<String,Object>();
			String code = "领取失败";
			String msg="哎呀，红包滑走了，请稍后再试。";
			Map<String,String> map=hongbaoService.command_word(mchid,openid,command_word);
			if(CodesEnum.SUCCESS.getCode().equals(map.get("code"))) {
				code="领取成功";
				msg="红包已奉上，回到公众号中收获惊喜吧。 ";
			}
			if(CodesEnum.FAIL0007.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg="您已领过啦，不要贪心哦。 ";
			}
			if(CodesEnum.FAIL0102.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg="哎呀，您来晚啦，活动已结束。  ";
			}
			if(CodesEnum.FAIL0103.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg="哎呀，您来早啦，活动尚未开始。  ";
			}
			if(CodesEnum.FAIL0008.getCode().equals(map.get("code"))) {
				code="领取失败";
				msg="密令错误。  ";
			}
			maprest.put("code", code);
			maprest.put("msg", msg);
			return maprest;
	}
	
	@ApiOperation("获取openid")
	@RequestMapping(value="/getOpenidByCode",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> getOpenidByCode(
			@RequestBody Map<String,Object> pammermap) throws IOException {
		String code=(String) pammermap.get("code");
		String state=(String) pammermap.get("state");
		logger.info("方法：getOpenidByCode（）入参:mchid="+code+"; "+"state="+state);
		
		Map<String, Object> maprest = hongbaoService.getOpenidByCode(code,state);
		
		if(StringUtil.isEmpty(maprest.get("openid"))) {
			maprest.put("code", "0002");
           	maprest.put("msg", maprest.get("errmsg"));
           	return maprest;
		}
		maprest.put("openid", maprest.get("openid"));
		maprest.put("code", "0000");
       	maprest.put("msg", "获取open成功");
		return maprest;
	}
	
	@ApiOperation("初始化用户信息")
	@RequestMapping(value="/initUserInfo",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> initUserInfo(
			@RequestBody Map<String,Object> pammermap) throws IOException {
		String openid=(String) pammermap.get("openid");
		String parent_openid=(String) pammermap.get("parent_openid");
		logger.info("方法：initUserInfo（）入参:openid="+openid+"; parent_openid:"+parent_openid);
		
		Map<String, Object> maprest = hongbaoService.initUserInfo(openid,parent_openid);
		logger.info("方法：initUserInfo（）出参数："+JSONUtils.toJSONString(maprest));
		return maprest;
	}
	
}
	
