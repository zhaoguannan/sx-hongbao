package com.sanxia.market.Controller;



import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sanxia.market.service.index.IndexService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author zgn 框架测试controller类
 *
 */

@Controller
@RequestMapping("/index")
@Api("入口")
public class IndexController {
	
	private final Logger logger = (Logger) LoggerFactory.getLogger(IndexController.class);
	
  
    
    @Autowired
    private IndexService service;
    
	
	@ApiOperation("入口")
	@RequestMapping(value="/SQ",method=RequestMethod.GET)
	public String receive(@RequestParam String mchid,@RequestParam String action
			) {
		logger.info("方法：SQ（）入参:mchid="+mchid+"; action="+action);
		String url=service.SQ( mchid,action);
		logger.info("输出SQurl:"+url);
	return "redirect:"+url;
	}
	
	
	@ApiOperation("授权回调")
	@RequestMapping(value="/redirect_uri",method=RequestMethod.GET)
	public String redirect_uri(@RequestParam("code") String code,@RequestParam("state") String state
			) {
		logger.info("方法：redirect_uri（）入参:code="+code+"; state="+state);
		String url=service.redirect_uri( code,state);
		logger.info("输出redirect_uriurl:"+url);
	return "redirect:"+url;
	}
	
	
	@ApiOperation("入口")
	@RequestMapping(value="/SQ2",method=RequestMethod.GET)
	@ResponseBody
	public Map receive2(@RequestParam String mchid,@RequestParam String action,@RequestParam String openid
			) {
		logger.info("方法：SQ2（）入参:mchid="+mchid+"; action="+action+";openid="+openid);
		String url=service.SQ2( mchid,action,openid);
		logger.info("输出方法：SQ2:"+url);
		Map m = new HashMap();
		m.put("url",url);
	return m;
	}
	
	
	@ApiOperation("授权回调")
	@RequestMapping(value="/redirect_uri2",method=RequestMethod.GET)
	public String redirect_uri2(@RequestParam("code") String code,@RequestParam("state") String state
			) {
		logger.info("方法：redirect_uri2（）入参:code="+code+"; state="+state);
		String url=service.redirect_uri2( code,state);
		logger.info("\"方法：redirect_uri2输出url2:"+url);
	return "redirect:"+url;
	}
	
	 
	
}
	
