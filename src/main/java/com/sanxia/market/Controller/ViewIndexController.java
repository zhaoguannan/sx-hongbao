package com.sanxia.market.Controller;


import com.sanxia.market.service.ViewIndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author zgn 框架测试controller类
 *
 */

@Controller
@RequestMapping("/box/index")
@Api("入口")
public class ViewIndexController {

	private final Logger logger = (Logger) LoggerFactory.getLogger(ViewIndexController.class);



    @Autowired
    private ViewIndexService service;


	@ApiOperation("微信授权入口")
	@RequestMapping(value="/SQ",method= RequestMethod.GET)
	public String receive(@RequestParam String mchid, @RequestParam String action, @RequestParam String busId
			) {
		logger.info("方法：SQ（）入参:mchid="+mchid+"; action="+action+"busId="+busId);
		String url=service.SQ( mchid,action,busId);
		logger.info("输出url:"+url);
	return "redirect:"+url;
	}


	@ApiOperation("授权回调")
	@RequestMapping(value="/redirect_uri",method= RequestMethod.GET)
	public String redirect_uri(@RequestParam("code") String code, @RequestParam("state") String state
			) {
		logger.info("方法：redirect_uri（）入参:code="+code+"; state="+state);
		String url=service.redirect_uri( code,state);
		logger.info("输出url:"+url);
	return "redirect:"+url;
	}
}

