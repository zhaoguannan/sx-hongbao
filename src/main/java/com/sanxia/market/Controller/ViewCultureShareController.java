package com.sanxia.market.Controller;



import java.io.IOException;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sanxia.common.utils.JSONUtils;
import com.sanxia.common.utils.SHA1;
import com.sanxia.market.service.ViewTicketsBussinessService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 *
 * @author zgn 框架测试controller类
 *
 */

@Controller
@RequestMapping("/box/share")
@Api("culture分享相关api")
public class ViewCultureShareController {

	private final Logger logger = (Logger) LoggerFactory.getLogger(ViewCultureShareController.class);


	@Autowired
	private ViewTicketsBussinessService service;


	@ApiOperation("微信分享页面")
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public String receive(@RequestParam("mchid") String mchid, @RequestParam(value = "openid", defaultValue = "") String openid, @RequestParam("busId") String busId, @RequestParam(value = "from", required = false) String from
			, Map<String, Object> maprest) throws IOException {
		logger.info("方法：culture/share:query（）入参:mchid=" + mchid + "; openid=" + openid + ";busId=" + busId + ";from=" + from);

		Map<String, Object> map = service.queryTickets(mchid, openid, busId);
		maprest.put("code", "贵宾号VIP：" + map.get("code"));
		maprest.put("openid", openid);
		maprest.put("mchid", mchid);
		 
		return "box_share";//"tickets1";
	}

	@ApiOperation("活动规则页面")
	@RequestMapping(value = "/share1", method = RequestMethod.GET)
	public String share1()  {
		logger.info("方法：culture/share:share1（）");

		return "share1";
	}

	@ApiOperation("获取微信分享参数")
	@RequestMapping(value = "/share", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, String> share(@RequestParam("mchid") String mchid, @RequestParam("openid") String openid, @RequestParam("busId") String busId, @RequestParam("pageType") String pageType, @RequestParam("code") String code, @RequestParam(value = "from", required = false) String from
			, Map<String, Object> maprest) throws IOException {
		logger.info("方法：culture/share:share（）入参:mchid=" + mchid + "; openid=" + openid + "; busId=" + busId + "pageType=:"+pageType+";code="+code+";from=" + from);
		Map<String, String> map = service.queryShare(mchid, openid, busId, pageType,code,from);
		logger.info("方法：culture/share:share（）出参:"+JSONUtils.toJSONString(map));
		return map;
	}
//
//	public static void main(String[] args) {
//
//		SortedMap<String,Object> parameters =new	TreeMap<String,Object>();
//		parameters.put("appId", "wx1be1cc389e8dffb9");
//		parameters.put("timestamp", "123456");
//		parameters.put("nonceStr", "090909");
//		//parameters.put("signature", value)
//	 String	sign=CreateSignUtil.createSignBalance("utf-8", parameters, "2a0f980625458e10644c351b96230e99");
//	 System.out.println(sign);
//
//	}

	public static void main(String[] args) {
		String accurl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
		String appid = "wx1be1cc389e8dffb9";
		String appsecret = "2a0f980625458e10644c351b96230e99";
		String requestUrl = accurl.replace("APPID", appid).replace(
				"APPSECRET", appsecret);

		//	String jsonString =HttpClientUtil.doGet(requestUrl);
		//	System.out.println(jsonString);
		//{"access_token":"22_eFicwu308DMmneLalsGXbykS5yDhGQ2tDJ1fKqWuQ7ho5usi7SdIC0243GpcMEQOn6b6Xpng-c4XQa8zxRQ8KcamBxOM7kv-VM0Lu5iGrLVXsQSVdeHsHnzoAeMb105jssDTKjIwBhVLvAJSYNEeAAAGVM","expires_in":7200}

		String api = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";
		String access_token = "22_eFicwu308DMmneLalsGXbykS5yDhGQ2tDJ1fKqWuQ7ho5usi7SdIC0243GpcMEQOn6b6Xpng-c4XQa8zxRQ8KcamBxOM7kv-VM0Lu5iGrLVXsQSVdeHsHnzoAeMb105jssDTKjIwBhVLvAJSYNEeAAAGVM";
		String apiurl = api.replace("ACCESS_TOKEN", access_token);
		//	String jsonString =HttpClientUtil.doGet(apiurl);
		//	System.out.println(jsonString);
		String ticket = "LIKLckvwlJT9cWIhEQTwfIiEX_Jq-w7Rs3cTWx2QYIhA6hCcCur1ism_FTUrYiRPgJId8SqSg9YVeyNMM62RhA";
		//{"errcode":0,"errmsg":"ok","ticket":"LIKLckvwlJT9cWIhEQTwfIiEX_Jq-w7Rs3cTWx2QYIhA6hCcCur1ism_FTUrYiRPgJId8SqSg9YVeyNMM62RhA","expires_in":7200}

		String noncestr = "Wm3WZYTPz0wzccnW";
//	String jsapi_ticket="sM4AOVdWfPE4DxkXGEs8VMCPGGVi4C3VM0P37wVUCFvkVAy_90u5h9nbSlYy3-Sl-HhTdfl2fzFy1AOcHKP7qg"
		String timestamp = "1414587457";
		String url = "http://mall.shijiebox.com/sx-hongbao/tickets/query?mchid=2&openid=oKvRh5oB_kWkH1NrgfXnszBisrLM";
		SortedMap<String, Object> parameters = new TreeMap<String, Object>();
		parameters.put("noncestr", noncestr);
		parameters.put("timestamp", timestamp);
		parameters.put("url", url);
		parameters.put("jsapi_ticket", ticket);
		String sign = SHA1.createSignBalance(parameters);
		System.out.println(sign);
		String sign1 = "e8f83c4309c725aba46ebfa418c330247adc881b";
	}


}

