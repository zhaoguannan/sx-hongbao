package com.sanxia.market.service;


import com.sanxia.common.utils.SHA1;
import com.sanxia.common.utils.StringUtil;
import com.sanxia.market.entity.UserShareLog;
import com.sanxia.market.entity.WxInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


@Service("ViewTicketsBussinessService")
public class ViewTicketsBussinessService {
	private final Logger logger = (Logger) LoggerFactory.getLogger(ViewTicketsBussinessService.class);



	@Autowired
	private UserService userService;

	@Autowired
	private UserShareLogService cultureUserShareLogPlusMapper;

	@Autowired
	private WxInfoService toolsWxConfigPlusMapper;


	public Map<String, Object> queryTickets(String mchid, String openid,String busId) {
		Map<String, Object> restmap=new HashMap<String,Object>();

		UserShareLog cultureUserShareLog=cultureUserShareLogPlusMapper.getById(busId);
		if(StringUtil.isEmpty(cultureUserShareLog)) {
			restmap.put("code", "0001");
			restmap.put("msg", "分享记录id 错误！");
			return restmap;
		}
		restmap.put("code", "0000");
		restmap.put("msg", "成功");
		return restmap;
	}

	public Map<String, String> queryShare(String mchid, String openid,String busId,String pageType, String code,String from) {
		Map<String, String>  map =new HashMap<String,String>();
		//{"errcode":0,"errmsg":"ok","ticket":"LIKLckvwlJT9cWIhEQTwfIiEX_Jq-w7Rs3cTWx2QYIhA6hCcCur1ism_FTUrYiRPgJId8SqSg9YVeyNMM62RhA","expires_in":7200}
		Map<String,Object> paramMap=new HashMap<String,Object>();
		if(StringUtil.isEmpty(mchid)) {
			paramMap.put("mchId", "2");
		}else {
			paramMap.put("mchId", mchid);
		}

		WxInfo toolsWxConfig=toolsWxConfigPlusMapper.getById(mchid);
		String ticket=toolsWxConfig.getJsapiTicket();
		String appId=toolsWxConfig.getWxAppid();
		String 	noncestr="Wm3WZYTPz0wzccnW";
		String timestamp="1414587457";
		String 	url="";
		logger.info("from="+from);
		
		if("1".equals(pageType)) {
			if(!StringUtil.isEmpty(from)) {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/home?mchid="+mchid+"&openid="+openid+"&busId="+busId+"&from="+from;
			}else {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/home?mchid="+mchid+"&openid="+openid+"&busId="+busId;
			}
		}else {
			if(!StringUtil.isEmpty(from)) {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/activity?mchid="+mchid+"&openid="+openid+"&busId="+busId+"&from="+from;
			}else {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/activity?mchid="+mchid+"&openid="+openid+"&busId="+busId;
			}
		}
		
		if("1".equals(pageType)) {
			if(!StringUtil.isEmpty(from)) {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/home?mchid="+mchid+"&openid="+openid+"&busId="+busId+"&from="+from;
			}else {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/home?mchid="+mchid+"&openid="+openid+"&busId="+busId;
			}
		}else {
			if(!StringUtil.isEmpty(from)) {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/activity?mchid="+mchid+"&openid="+openid+"&busId="+busId+"&from="+from;
			}else {
			 url="http://share.shijiebox.com/?code="+code+"&state=123#/activity?mchid="+mchid+"&openid="+openid+"&busId="+busId;
			}
		}
		
		//url="http://share.shijiebox.com/?code="+code+"&state=123#/activity?mchid="+mchid+"&openid="+openid+"&busId="+busId;
		
		logger.info("url:"+url);
		SortedMap<String,Object> parameters =new TreeMap<String,Object>();
		parameters.put("noncestr", noncestr);
		parameters.put("timestamp", timestamp);
		parameters.put("url", url);
		parameters.put("jsapi_ticket", ticket);
		String sign= SHA1.createSignBalance(parameters);
		map.put("noncestr", noncestr);
		map.put("appId", appId);
		map.put("timestamp", timestamp);
		map.put("signature", sign);
		map.put("url", url);
		return map;
	}


}
