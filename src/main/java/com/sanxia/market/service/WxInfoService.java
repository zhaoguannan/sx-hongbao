package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.WxInfo;

 /**   
* @Title: WxInfoService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-06-19 19:41:31
* @version V1.0   
* create by codeFactory
*/
public interface WxInfoService extends BaseService<WxInfo>{


	}
