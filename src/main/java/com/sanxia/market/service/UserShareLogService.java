package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.UserShareLog;

 /**   
* @Title: UserShareLogService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-09-27 10:24:10
* @version V1.0   
* create by codeFactory
*/
public interface UserShareLogService extends BaseService<UserShareLog>{


	}
