package com.sanxia.market.service;



import com.sanxia.common.utils.HttpClientUtil;
import com.sanxia.common.utils.JsonUtil;
import com.sanxia.common.utils.StringUtil;
import com.sanxia.market.entity.HbAction;
import com.sanxia.market.entity.User;
import com.sanxia.market.entity.UserShareLog;
import com.sanxia.market.entity.WxInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
* @Title: HongbaoLogService.java
* @Package com.sanxia.market.service
* @Description:
* @author 赵贯男
* @date 2019-01-18 13:33:52
* @version V1.0
* create by codeFactory
*/
@Service("ViewIndexService")
public class ViewIndexService {

	@Autowired
	private WxInfoService toolsWxConfigPlusMapper;


	@Autowired
	private UserService baseUserPlusMapper;

	@Autowired
	private HbActionService toolsActionPlusMapper;

	@Autowired
	private UserShareLogService  cultureUserShareLogPlusMapper;


	public String SQ(String mchid, String action,String busId) {
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("mchId", mchid);
		WxInfo toolsWxConfig=toolsWxConfigPlusMapper.getBy(paramMap);

	String wx_appid=toolsWxConfig.getWxAppid();
	String mch_name = toolsWxConfig.getMchName();
	String url="https://open.weixin.qq.com/connect/oauth2/authorize?";
	String redirect_uri="http://share.shijiebox.com/api/index/redirect_uri";
	//appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect//snsapi_userinfo snsapi_base
	url=url+"appid="+wx_appid+"&redirect_uri="+redirect_uri+"&response_type=code&scope=snsapi_userinfo&state="+action+","+mchid+","+busId+"#wechat_redirect";
		return url;
}

	@Transactional
	public String redirect_uri(String code, String state) {
		String[] sta=state.split(",");
		String action=sta[0];
		String mchid=sta[1];
		String busId=sta[2];
//		Map<String,Object> paramMap=new HashMap<String,Object>();
//		paramMap.put("mchId", mchid);
		Map<String,Object> paramMap=new HashMap<>();
		paramMap.put("mch_id",mchid);
		WxInfo	wxInfo=toolsWxConfigPlusMapper.getBy(paramMap);
		String url="https://api.weixin.qq.com/sns/oauth2/access_token?appid="+wxInfo.getWxAppid()+"&secret="+wxInfo.getWxAppSecret()+"&code="+code+"&grant_type=authorization_code";
		String rest= HttpClientUtil.doGet(url);
		Map map=(Map) JsonUtil.jsonStr2Bean(rest, Map.class);
		String openid=(String) map.get("openid");

		//paramMap.clear();
	//	paramMap.put("action", action);
		Map<String,Object> tparamMap=new HashMap<String,Object>();
		tparamMap.put("action",action);
		HbAction toolsAction=toolsActionPlusMapper.getBy(tparamMap);


		//查询用户openid是否存在
		Map<String,Object> uparamMap=new HashMap<String,Object>();
		uparamMap.put("wx_openid",openid);
		User cultureUser=baseUserPlusMapper.getBy(uparamMap);

		//查询分享记录是否存在
		UserShareLog cultureUserShareLog=cultureUserShareLogPlusMapper.getById(busId);

		User cultureUserParent=baseUserPlusMapper.getById(cultureUserShareLog.getUserId());

		if(StringUtil.isEmpty(cultureUser)) {
			//创建base用户
			cultureUser=new User();
			cultureUser.setUserOpenid(openid);
			cultureUser.setParentUserId(cultureUserParent.getId()+"");
			cultureUser.setAddTime(new Date());
			baseUserPlusMapper.insertOne(cultureUser);

		}else{

		}

		//创建分享记录
		UserShareLog cultureUserShareLog1=new UserShareLog();
		cultureUserShareLog1.setCreateTime(new Date());
	//	cultureUserShareLog1.setLinkId(cultureUserShareLog.getCulLinkId());
		cultureUserShareLog1.setUserId(cultureUser.getId()+"");
		cultureUserShareLog1.setLogType(cultureUserShareLog.getLogType());
		cultureUserShareLog1.setShareUrl("");
		cultureUserShareLog1.setCreateBy(cultureUser.getId()+"");
		cultureUserShareLog1.setReserveFields1(cultureUserShareLog.getReserveFields1());
		cultureUserShareLogPlusMapper.insertOne(cultureUserShareLog1);

         //更改分享id
		busId=cultureUserShareLog1.getId()+"";

		return toolsAction.getUrl()+"?mchid="+mchid+"&openid="+openid+"&busId="+busId;
	}


	}
