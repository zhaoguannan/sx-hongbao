package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.HbAction;

 /**   
* @Title: HbActionService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-01-18 14:16:23
* @version V1.0   
* create by codeFactory
*/
public interface HbActionService extends BaseService<HbAction>{


	}
