package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.WxHongbaoInfo;

 /**   
* @Title: WxHongbaoInfoService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-01-24 11:11:02
* @version V1.0   
* create by codeFactory
*/
public interface WxHongbaoInfoService extends BaseService<WxHongbaoInfo>{


	}
