package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.HongbaoLog;

 /**   
* @Title: HongbaoLogService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-09-27 19:59:12
* @version V1.0   
* create by codeFactory
*/
public interface HongbaoLogService extends BaseService<HongbaoLog>{


	}
