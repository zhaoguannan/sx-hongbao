package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.CommandWord;

 /**   
* @Title: CommandWordService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-01-24 20:20:21
* @version V1.0   
* create by codeFactory
*/
public interface CommandWordService extends BaseService<CommandWord>{


	}
