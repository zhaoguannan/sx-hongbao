package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.Tickets;

 /**   
* @Title: TicketsService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-06-18 16:27:20
* @version V1.0   
* create by codeFactory
*/
public interface TicketsService extends BaseService<Tickets>{


	}
