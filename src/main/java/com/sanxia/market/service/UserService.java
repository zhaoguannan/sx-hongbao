package com.sanxia.market.service;
import java.util.List;

import com.sanxia.common.core.service.BaseService;
import com.sanxia.market.entity.User;

 /**   
* @Title: UserService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-09-27 10:40:54
* @version V1.0   
* create by codeFactory
*/
public interface UserService extends BaseService<User>{


	}
