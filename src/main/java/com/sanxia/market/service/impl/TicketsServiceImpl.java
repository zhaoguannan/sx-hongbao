package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.Tickets;
import com.sanxia.market.service.TicketsService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: TicketsService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-06-18 16:27:20
* @version V1.0   
* create by codeFactory
*/
@Service("TicketsServiceImpl")
public class TicketsServiceImpl extends BaseServiceImpl<Tickets>  implements TicketsService{


	}
