package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.WxInfo;
import com.sanxia.market.service.WxInfoService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: WxInfoService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-06-19 19:41:31
* @version V1.0   
* create by codeFactory
*/
@Service("WxInfoServiceImpl")
public class WxInfoServiceImpl extends BaseServiceImpl<WxInfo>  implements WxInfoService{


	}
