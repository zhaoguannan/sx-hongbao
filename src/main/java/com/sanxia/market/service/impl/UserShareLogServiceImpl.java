package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.UserShareLog;
import com.sanxia.market.service.UserShareLogService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: UserShareLogService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-09-27 10:24:10
* @version V1.0   
* create by codeFactory
*/
@Service("UserShareLogServiceImpl")
public class UserShareLogServiceImpl extends BaseServiceImpl<UserShareLog>  implements UserShareLogService{


	}
