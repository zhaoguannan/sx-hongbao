package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.User;
import com.sanxia.market.service.UserService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: UserService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-09-27 10:40:54
* @version V1.0   
* create by codeFactory
*/
@Service("UserServiceImpl")
public class UserServiceImpl extends BaseServiceImpl<User>  implements UserService{


	}
