package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.WxHongbaoInfo;
import com.sanxia.market.service.WxHongbaoInfoService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: WxHongbaoInfoService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-01-24 11:11:02
* @version V1.0   
* create by codeFactory
*/
@Service("WxHongbaoInfoServiceImpl")
public class WxHongbaoInfoServiceImpl extends BaseServiceImpl<WxHongbaoInfo>  implements WxHongbaoInfoService{


	}
