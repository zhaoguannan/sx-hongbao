package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.HbAction;
import com.sanxia.market.service.HbActionService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: HbActionService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-01-18 14:16:23
* @version V1.0   
* create by codeFactory
*/
@Service("HbActionServiceImpl")
public class HbActionServiceImpl extends BaseServiceImpl<HbAction>  implements HbActionService{


	}
