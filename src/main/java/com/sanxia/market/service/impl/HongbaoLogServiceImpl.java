package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.HongbaoLog;
import com.sanxia.market.service.HongbaoLogService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: HongbaoLogService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-09-27 19:59:12
* @version V1.0   
* create by codeFactory
*/
@Service("HongbaoLogServiceImpl")
public class HongbaoLogServiceImpl extends BaseServiceImpl<HongbaoLog>  implements HongbaoLogService{


	}
