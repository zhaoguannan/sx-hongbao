package com.sanxia.market.service.impl;

import com.sanxia.common.core.service.BaseServiceImpl;
import com.sanxia.market.entity.CommandWord;
import com.sanxia.market.service.CommandWordService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


 

 /**   
* @Title: CommandWordService.java 
* @Package com.sanxia.market.service
* @Description: 
* @author 赵贯男
* @date 2019-01-24 20:20:21
* @version V1.0   
* create by codeFactory
*/
@Service("CommandWordServiceImpl")
public class CommandWordServiceImpl extends BaseServiceImpl<CommandWord>  implements CommandWordService{


	}
