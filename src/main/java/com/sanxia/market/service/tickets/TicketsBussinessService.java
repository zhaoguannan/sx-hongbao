package com.sanxia.market.service.tickets;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sanxia.common.enums.CodesEnum;
import com.sanxia.common.utils.SHA1;
import com.sanxia.common.utils.StringUtil;
import com.sanxia.market.Controller.HongbaoController;
import com.sanxia.market.entity.Tickets;
import com.sanxia.market.entity.User;
import com.sanxia.market.entity.WxInfo;
import com.sanxia.market.service.TicketsService;
import com.sanxia.market.service.UserService;
import com.sanxia.market.service.WxInfoService;

@Service("TicketsBussinessService")
public class TicketsBussinessService {
	private final Logger logger = (Logger) LoggerFactory.getLogger(TicketsBussinessService.class);
	
	  

	@Autowired
	private UserService userService;
	
	@Autowired
	private TicketsService ticketsService;
	
	@Autowired
	private WxInfoService wxInfoService;
	
	public Map<String, String> queryTickets(String mchid, String openid) {
		Map<String, String> restmap=new HashMap<String,String>();
		Map<String,Object> paramMap=new HashMap<String,Object>();
		paramMap.put("userOpenid", openid);
		paramMap.put("mechId", mchid);//merch_id
		
		Tickets tickets=ticketsService.getBy(paramMap);
		if(StringUtil.isEmpty(tickets)) {
			User user=userService.getBy(paramMap);
			tickets=new Tickets();
			tickets.setAddTime(new Date());
			tickets.setMerchId(mchid);
			tickets.setUserId(user.getId()+"");
			tickets.setUserOpenid(openid);
			tickets.setTicketsNo((1000+user.getId())+"");
			ticketsService.insertOne(tickets);
		}
	
		logger.info("入场券编号为："+tickets.getTicketsNo());
		restmap.put("code", tickets.getTicketsNo());
		restmap.put("msg", "成功");
		return restmap;
	}

	public Map<String, String> queryShare( String openid,String url) {
		Map<String, String>  map =new HashMap<String,String>();
		
		String mchid = "2";
		//{"errcode":0,"errmsg":"ok","ticket":"LIKLckvwlJT9cWIhEQTwfIiEX_Jq-w7Rs3cTWx2QYIhA6hCcCur1ism_FTUrYiRPgJId8SqSg9YVeyNMM62RhA","expires_in":7200}
		Map<String,Object> paramMap=new HashMap<String,Object>();
		if(StringUtil.isEmpty(mchid)) {
			paramMap.put("mchId", "2");
		}else {
			paramMap.put("mchId", mchid);
		}
		
		WxInfo	wxInfo=wxInfoService.getBy(paramMap);
		String ticket=wxInfo.getJsapiTicket();
		String appId=wxInfo.getWxAppid();
		String 	noncestr="Wm3WZYTPz0wzccnW";
		String timestamp="1414587457";
		//String 	url="";
		//url="http://share.shijiebox.com/index/SQ?mchid=2&";
//		 url="http://share.shijiebox.com/?from=singlemessage#/activity?mchid="+mchid+"&openid="+openid;	
//		 String url1="http://share.shijiebox.com/#/activity?mchid="+mchid+"&openid="+openid;	
//		
		SortedMap<String,Object> parameters =new TreeMap<String,Object>();
		parameters.put("noncestr", noncestr);
		parameters.put("timestamp", timestamp);
		parameters.put("url", url);
		parameters.put("jsapi_ticket", ticket);
		String sign=SHA1.createSignBalance(parameters);
		
		parameters.clear();
		parameters =new TreeMap<String,Object>();
		parameters.put("noncestr", noncestr);
		parameters.put("timestamp", timestamp);
		parameters.put("url", url);
		parameters.put("jsapi_ticket", ticket);
		String sign2=SHA1.createSignBalance(parameters);
		
		map.put("noncestr", noncestr);
		map.put("appId", appId);
		map.put("timestamp", timestamp);
		map.put("signature", sign);
		map.put("signature2", sign2);
		map.put("url",url);
		return map;
	}

}
