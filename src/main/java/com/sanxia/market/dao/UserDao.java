package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.User;



/**   
* @Title: UserDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-09-27 10:40:54
* @version V1.0   
* create by codeFactory
*/
public interface UserDao extends BaseDao<User> {

}