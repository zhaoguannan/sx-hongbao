package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.HongbaoLog;



/**   
* @Title: HongbaoLogDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-09-27 19:59:12
* @version V1.0   
* create by codeFactory
*/
public interface HongbaoLogDao extends BaseDao<HongbaoLog> {

}