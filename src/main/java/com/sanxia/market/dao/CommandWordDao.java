package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.CommandWord;



/**   
* @Title: CommandWordDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-01-24 20:20:21
* @version V1.0   
* create by codeFactory
*/
public interface CommandWordDao extends BaseDao<CommandWord> {

}