package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.HbAction;



/**   
* @Title: HbActionDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-01-18 14:16:23
* @version V1.0   
* create by codeFactory
*/
public interface HbActionDao extends BaseDao<HbAction> {

}