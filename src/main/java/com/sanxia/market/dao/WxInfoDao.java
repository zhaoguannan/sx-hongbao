package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.WxInfo;



/**   
* @Title: WxInfoDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-06-19 19:41:31
* @version V1.0   
* create by codeFactory
*/
public interface WxInfoDao extends BaseDao<WxInfo> {

}