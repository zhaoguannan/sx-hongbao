package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.Tickets;



/**   
* @Title: TicketsDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-06-18 16:27:20
* @version V1.0   
* create by codeFactory
*/
public interface TicketsDao extends BaseDao<Tickets> {

}