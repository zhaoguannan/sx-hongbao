package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.WxHongbaoInfo;



/**   
* @Title: WxHongbaoInfoDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-01-24 11:11:02
* @version V1.0   
* create by codeFactory
*/
public interface WxHongbaoInfoDao extends BaseDao<WxHongbaoInfo> {

}