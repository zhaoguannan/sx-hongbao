package com.sanxia.market.dao;

import com.sanxia.common.core.dao.BaseDao;
import com.sanxia.market.entity.UserShareLog;



/**   
* @Title: UserShareLogDao.java 
* @Package com.sanxia.market.dao
* @Description: 
* @author 赵贯男
* @date 2019-09-27 10:24:10
* @version V1.0   
* create by codeFactory
*/
public interface UserShareLogDao extends BaseDao<UserShareLog> {

}