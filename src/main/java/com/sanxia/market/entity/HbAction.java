package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
* @TableName: hb_action 
* @Package: com.sanxia.market.entity
* @Title:HbAction.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-01-18 14:16:23
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class HbAction extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :
	*/
	@ApiModelProperty(value = "" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields action :请求类型
	*/
	@ApiModelProperty(value = "请求类型" ,dataType = "String") 
	private String action;
	/**
	*@Fields url :请求地址
	*/
	@ApiModelProperty(value = "请求地址" ,dataType = "String") 
	private String url;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setAction(String action){
		this.action=action;
	}

	public String getAction(){
		return action;
	}
	public void setUrl(String url){
		this.url=url;
	}

	public String getUrl(){
		return url;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("  id= "+id+",");
	}
	if(null != action && !"".equals(action) ){
		sb.append("请求类型  action= "+action+",");
	}
	if(null != url && !"".equals(url) ){
		sb.append("请求地址  url= "+url+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
