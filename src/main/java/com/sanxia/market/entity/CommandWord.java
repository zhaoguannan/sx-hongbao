package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
* @TableName: command_word 
* @Package: com.sanxia.market.entity
* @Title:CommandWord.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-01-24 20:20:20
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class CommandWord extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :
	*/
	@ApiModelProperty(value = "" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields commandWord :口令
	*/
	@ApiModelProperty(value = "口令" ,dataType = "String") 
	private String commandWord;
	/**
	*@Fields giveNumber :发放个数（如果为-100则不限制数量）
	*/
	@ApiModelProperty(value = "发放个数（如果为-100则不限制数量）" ,dataType = "Long") 
	private Long giveNumber;
	/**
	*@Fields wxHongbaoInfoId :红包类型id
	*/
	@ApiModelProperty(value = "红包类型id" ,dataType = "Long") 
	private Long wxHongbaoInfoId;
	/**
	*@Fields commandKey :加密key(根据口令和key做MD5验签)
	*/
	@ApiModelProperty(value = "加密key(根据口令和key做MD5验签)" ,dataType = "String") 
	private String commandKey;
	/**
	*@Fields question :问题
	*/
	@ApiModelProperty(value = "问题" ,dataType = "String") 
	private String question;
	/**
	*@Fields commandType :口令类型（pass只需要口令，ques需要口令和问题）
	*/
	@ApiModelProperty(value = "口令类型（pass只需要口令，ques需要口令和问题）" ,dataType = "String") 
	private String commandType;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setCommandWord(String commandWord){
		this.commandWord=commandWord;
	}

	public String getCommandWord(){
		return commandWord;
	}
	public void setGiveNumber(Long giveNumber){
		this.giveNumber=giveNumber;
	}

	public Long getGiveNumber(){
		return giveNumber;
	}
	public void setWxHongbaoInfoId(Long wxHongbaoInfoId){
		this.wxHongbaoInfoId=wxHongbaoInfoId;
	}

	public Long getWxHongbaoInfoId(){
		return wxHongbaoInfoId;
	}
	public void setCommandKey(String commandKey){
		this.commandKey=commandKey;
	}

	public String getCommandKey(){
		return commandKey;
	}
	public void setQuestion(String question){
		this.question=question;
	}

	public String getQuestion(){
		return question;
	}
	public void setCommandType(String commandType){
		this.commandType=commandType;
	}

	public String getCommandType(){
		return commandType;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("  id= "+id+",");
	}
	if(null != commandWord && !"".equals(commandWord) ){
		sb.append("口令  commandWord= "+commandWord+",");
	}
	if(null != giveNumber && !"".equals(giveNumber) ){
		sb.append("发放个数（如果为-100则不限制数量）  giveNumber= "+giveNumber+",");
	}
	if(null != wxHongbaoInfoId && !"".equals(wxHongbaoInfoId) ){
		sb.append("红包类型id  wxHongbaoInfoId= "+wxHongbaoInfoId+",");
	}
	if(null != commandKey && !"".equals(commandKey) ){
		sb.append("加密key(根据口令和key做MD5验签)  commandKey= "+commandKey+",");
	}
	if(null != question && !"".equals(question) ){
		sb.append("问题  question= "+question+",");
	}
	if(null != commandType && !"".equals(commandType) ){
		sb.append("口令类型（pass只需要口令，ques需要口令和问题）  commandType= "+commandType+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
