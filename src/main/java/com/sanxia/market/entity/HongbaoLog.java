package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.*;

/**
* @TableName: hongbao_log 
* @Package: com.sanxia.market.entity
* @Title:HongbaoLog.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-09-27 19:59:12
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class HongbaoLog extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :
	*/
	@ApiModelProperty(value = "" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields logNo :红包商户订单号
	*/
	@ApiModelProperty(value = "红包商户订单号" ,dataType = "String") 
	private String logNo;
	/**
	*@Fields sendListid :微信单号
	*/
	@ApiModelProperty(value = "微信单号" ,dataType = "String") 
	private String sendListid;
	/**
	*@Fields logStatus :订单状态（0失败1成功）
	*/
	@ApiModelProperty(value = "订单状态（0失败1成功）" ,dataType = "String") 
	private String logStatus;
	/**
	*@Fields totalAmount :付款金额，单位分
	*/
	@ApiModelProperty(value = "付款金额，单位分" ,dataType = "Integer") 
	private Integer totalAmount;
	/**
	*@Fields mchId :平台商户id
	*/
	@ApiModelProperty(value = "平台商户id" ,dataType = "String") 
	private String mchId;
	/**
	*@Fields userOpenid :用户openid
	*/
	@ApiModelProperty(value = "用户openid" ,dataType = "String") 
	private String userOpenid;
	/**
	*@Fields userId :用户id
	*/
	@ApiModelProperty(value = "用户id" ,dataType = "Long") 
	private Long userId;
	/**
	*@Fields logDesc :微信返回结果
	*/
	@ApiModelProperty(value = "微信返回结果" ,dataType = "String") 
	private String logDesc;
	/**
	*@Fields addTime :记录创建时间
	*/
	@ApiModelProperty(value = "记录创建时间" ,dataType = "Date") 
	private Date addTime;
	/**
	*@Fields hongbaoType :红包类型
	*/
	@ApiModelProperty(value = "红包类型" ,dataType = "String") 
	private String hongbaoType;
	/**
	*@Fields reserveFields1 :预留字段1
	*/
	@ApiModelProperty(value = "预留字段1" ,dataType = "String") 
	private String reserveFields1;
	/**
	*@Fields reserveFields2 :预留字段2
	*/
	@ApiModelProperty(value = "预留字段2" ,dataType = "String") 
	private String reserveFields2;
	/**
	*@Fields reserveFields3 :预留字段3
	*/
	@ApiModelProperty(value = "预留字段3" ,dataType = "String") 
	private String reserveFields3;
	/**
	*@Fields reserveFields4 :预留字段4
	*/
	@ApiModelProperty(value = "预留字段4" ,dataType = "String") 
	private String reserveFields4;
	/**
	*@Fields reserveFields5 :预留字段5
	*/
	@ApiModelProperty(value = "预留字段5" ,dataType = "String") 
	private String reserveFields5;
	/**
	*@Fields reserveFields6 :预留字段6
	*/
	@ApiModelProperty(value = "预留字段6" ,dataType = "String") 
	private String reserveFields6;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setLogNo(String logNo){
		this.logNo=logNo;
	}

	public String getLogNo(){
		return logNo;
	}
	public void setSendListid(String sendListid){
		this.sendListid=sendListid;
	}

	public String getSendListid(){
		return sendListid;
	}
	public void setLogStatus(String logStatus){
		this.logStatus=logStatus;
	}

	public String getLogStatus(){
		return logStatus;
	}
	public void setTotalAmount(Integer totalAmount){
		this.totalAmount=totalAmount;
	}

	public Integer getTotalAmount(){
		return totalAmount;
	}
	public void setMchId(String mchId){
		this.mchId=mchId;
	}

	public String getMchId(){
		return mchId;
	}
	public void setUserOpenid(String userOpenid){
		this.userOpenid=userOpenid;
	}

	public String getUserOpenid(){
		return userOpenid;
	}
	public void setUserId(Long userId){
		this.userId=userId;
	}

	public Long getUserId(){
		return userId;
	}
	public void setLogDesc(String logDesc){
		this.logDesc=logDesc;
	}

	public String getLogDesc(){
		return logDesc;
	}
	public void setAddTime(Date addTime){
		this.addTime=addTime;
	}

	public Date getAddTime(){
		return addTime;
	}
	public void setHongbaoType(String hongbaoType){
		this.hongbaoType=hongbaoType;
	}

	public String getHongbaoType(){
		return hongbaoType;
	}
	public void setReserveFields1(String reserveFields1){
		this.reserveFields1=reserveFields1;
	}

	public String getReserveFields1(){
		return reserveFields1;
	}
	public void setReserveFields2(String reserveFields2){
		this.reserveFields2=reserveFields2;
	}

	public String getReserveFields2(){
		return reserveFields2;
	}
	public void setReserveFields3(String reserveFields3){
		this.reserveFields3=reserveFields3;
	}

	public String getReserveFields3(){
		return reserveFields3;
	}
	public void setReserveFields4(String reserveFields4){
		this.reserveFields4=reserveFields4;
	}

	public String getReserveFields4(){
		return reserveFields4;
	}
	public void setReserveFields5(String reserveFields5){
		this.reserveFields5=reserveFields5;
	}

	public String getReserveFields5(){
		return reserveFields5;
	}
	public void setReserveFields6(String reserveFields6){
		this.reserveFields6=reserveFields6;
	}

	public String getReserveFields6(){
		return reserveFields6;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("  id= "+id+",");
	}
	if(null != logNo && !"".equals(logNo) ){
		sb.append("红包商户订单号  logNo= "+logNo+",");
	}
	if(null != sendListid && !"".equals(sendListid) ){
		sb.append("微信单号  sendListid= "+sendListid+",");
	}
	if(null != logStatus && !"".equals(logStatus) ){
		sb.append("订单状态（0失败1成功）  logStatus= "+logStatus+",");
	}
	if(null != totalAmount && !"".equals(totalAmount) ){
		sb.append("付款金额，单位分  totalAmount= "+totalAmount+",");
	}
	if(null != mchId && !"".equals(mchId) ){
		sb.append("平台商户id  mchId= "+mchId+",");
	}
	if(null != userOpenid && !"".equals(userOpenid) ){
		sb.append("用户openid  userOpenid= "+userOpenid+",");
	}
	if(null != userId && !"".equals(userId) ){
		sb.append("用户id  userId= "+userId+",");
	}
	if(null != logDesc && !"".equals(logDesc) ){
		sb.append("微信返回结果  logDesc= "+logDesc+",");
	}
	if(null != addTime && !"".equals(addTime) ){
		sb.append("记录创建时间  addTime= "+addTime+",");
	}
	if(null != hongbaoType && !"".equals(hongbaoType) ){
		sb.append("红包类型  hongbaoType= "+hongbaoType+",");
	}
	if(null != reserveFields1 && !"".equals(reserveFields1) ){
		sb.append("预留字段1  reserveFields1= "+reserveFields1+",");
	}
	if(null != reserveFields2 && !"".equals(reserveFields2) ){
		sb.append("预留字段2  reserveFields2= "+reserveFields2+",");
	}
	if(null != reserveFields3 && !"".equals(reserveFields3) ){
		sb.append("预留字段3  reserveFields3= "+reserveFields3+",");
	}
	if(null != reserveFields4 && !"".equals(reserveFields4) ){
		sb.append("预留字段4  reserveFields4= "+reserveFields4+",");
	}
	if(null != reserveFields5 && !"".equals(reserveFields5) ){
		sb.append("预留字段5  reserveFields5= "+reserveFields5+",");
	}
	if(null != reserveFields6 && !"".equals(reserveFields6) ){
		sb.append("预留字段6  reserveFields6= "+reserveFields6+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
