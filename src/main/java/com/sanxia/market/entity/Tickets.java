package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.*;

/**
* @TableName: tickets 
* @Package: com.sanxia.market.entity
* @Title:Tickets.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-06-18 16:27:19
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class Tickets extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :入场券表
	*/
	@ApiModelProperty(value = "入场券表" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields ticketsNo :入场券编号
	*/
	@ApiModelProperty(value = "入场券编号" ,dataType = "String") 
	private String ticketsNo;
	/**
	*@Fields userId :用户id
	*/
	@ApiModelProperty(value = "用户id" ,dataType = "String") 
	private String userId;
	/**
	*@Fields userOpenid :用户openid
	*/
	@ApiModelProperty(value = "用户openid" ,dataType = "String") 
	private String userOpenid;
	/**
	*@Fields merchId :商户id
	*/
	@ApiModelProperty(value = "商户id" ,dataType = "String") 
	private String merchId;
	/**
	*@Fields addTime :添加时间
	*/
	@ApiModelProperty(value = "添加时间" ,dataType = "Date") 
	private Date addTime;
	/**
	*@Fields parentOpenid :父级openid
	*/
	@ApiModelProperty(value = "父级openid" ,dataType = "String") 
	private String parentOpenid;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setTicketsNo(String ticketsNo){
		this.ticketsNo=ticketsNo;
	}

	public String getTicketsNo(){
		return ticketsNo;
	}
	public void setUserId(String userId){
		this.userId=userId;
	}

	public String getUserId(){
		return userId;
	}
	public void setUserOpenid(String userOpenid){
		this.userOpenid=userOpenid;
	}

	public String getUserOpenid(){
		return userOpenid;
	}
	public void setMerchId(String merchId){
		this.merchId=merchId;
	}

	public String getMerchId(){
		return merchId;
	}
	public void setAddTime(Date addTime){
		this.addTime=addTime;
	}

	public Date getAddTime(){
		return addTime;
	}
	public void setParentOpenid(String parentOpenid){
		this.parentOpenid=parentOpenid;
	}

	public String getParentOpenid(){
		return parentOpenid;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("入场券表  id= "+id+",");
	}
	if(null != ticketsNo && !"".equals(ticketsNo) ){
		sb.append("入场券编号  ticketsNo= "+ticketsNo+",");
	}
	if(null != userId && !"".equals(userId) ){
		sb.append("用户id  userId= "+userId+",");
	}
	if(null != userOpenid && !"".equals(userOpenid) ){
		sb.append("用户openid  userOpenid= "+userOpenid+",");
	}
	if(null != merchId && !"".equals(merchId) ){
		sb.append("商户id  merchId= "+merchId+",");
	}
	if(null != addTime && !"".equals(addTime) ){
		sb.append("添加时间  addTime= "+addTime+",");
	}
	if(null != parentOpenid && !"".equals(parentOpenid) ){
		sb.append("父级openid  parentOpenid= "+parentOpenid+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
