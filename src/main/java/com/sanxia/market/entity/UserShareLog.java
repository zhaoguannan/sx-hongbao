package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.*;

/**
* @TableName: user_share_log 
* @Package: com.sanxia.market.entity
* @Title:UserShareLog.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-09-27 10:24:09
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class UserShareLog extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :用户分享日志id
	*/
	@ApiModelProperty(value = "用户分享日志id" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields logType :0影视宣发1版权投资
	*/
	@ApiModelProperty(value = "0影视宣发1版权投资" ,dataType = "String") 
	private String logType;
	/**
	*@Fields shareUrl :分享连接
	*/
	@ApiModelProperty(value = "分享连接" ,dataType = "String") 
	private String shareUrl;
	/**
	*@Fields userId :分享文娱头条用户id
	*/
	@ApiModelProperty(value = "分享文娱头条用户id" ,dataType = "String") 
	private String userId;
	/**
	*@Fields linkId :宣发或投资id
	*/
	@ApiModelProperty(value = "宣发或投资id" ,dataType = "String") 
	private String linkId;
	/**
	*@Fields isProceeds :本次分享是否产生收益(0 否 1 是)
	*/
	@ApiModelProperty(value = "本次分享是否产生收益(0 否 1 是)" ,dataType = "String") 
	private String isProceeds;
	/**
	*@Fields shareProceeds :分享产生的收益金额
	*/
	@ApiModelProperty(value = "分享产生的收益金额" ,dataType = "String") 
	private String shareProceeds;
	/**
	*@Fields enabled :使用状态(0正常1禁用)
	*/
	@ApiModelProperty(value = "使用状态(0正常1禁用)" ,dataType = "String") 
	private String enabled;
	/**
	*@Fields orgId :所属组织id
	*/
	@ApiModelProperty(value = "所属组织id" ,dataType = "String") 
	private String orgId;
	/**
	*@Fields createBy :创建人id
	*/
	@ApiModelProperty(value = "创建人id" ,dataType = "String") 
	private String createBy;
	/**
	*@Fields isDelete :0正常1删除
	*/
	@ApiModelProperty(value = "0正常1删除" ,dataType = "String") 
	private String isDelete;
	/**
	*@Fields createTime :添加时间
	*/
	@ApiModelProperty(value = "添加时间" ,dataType = "Date") 
	private Date createTime;
	/**
	*@Fields updateBy :修改人id
	*/
	@ApiModelProperty(value = "修改人id" ,dataType = "String") 
	private String updateBy;
	/**
	*@Fields updateTime :修改时间
	*/
	@ApiModelProperty(value = "修改时间" ,dataType = "Date") 
	private Date updateTime;
	/**
	*@Fields reserveFields1 :分享标题
	*/
	@ApiModelProperty(value = "分享标题" ,dataType = "String") 
	private String reserveFields1;
	/**
	*@Fields reserveFields2 :点击量
	*/
	@ApiModelProperty(value = "点击量" ,dataType = "String") 
	private String reserveFields2;
	/**
	*@Fields reserveFields3 :预留字段3
	*/
	@ApiModelProperty(value = "预留字段3" ,dataType = "String") 
	private String reserveFields3;
	/**
	*@Fields reserveFields4 :预留字段4
	*/
	@ApiModelProperty(value = "预留字段4" ,dataType = "String") 
	private String reserveFields4;
	/**
	*@Fields reserveFields5 :预留字段5
	*/
	@ApiModelProperty(value = "预留字段5" ,dataType = "String") 
	private String reserveFields5;
	/**
	*@Fields reserveFields6 :预留字段6
	*/
	@ApiModelProperty(value = "预留字段6" ,dataType = "String") 
	private String reserveFields6;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setLogType(String logType){
		this.logType=logType;
	}

	public String getLogType(){
		return logType;
	}
	public void setShareUrl(String shareUrl){
		this.shareUrl=shareUrl;
	}

	public String getShareUrl(){
		return shareUrl;
	}
	public void setUserId(String userId){
		this.userId=userId;
	}

	public String getUserId(){
		return userId;
	}
	public void setLinkId(String linkId){
		this.linkId=linkId;
	}

	public String getLinkId(){
		return linkId;
	}
	public void setIsProceeds(String isProceeds){
		this.isProceeds=isProceeds;
	}

	public String getIsProceeds(){
		return isProceeds;
	}
	public void setShareProceeds(String shareProceeds){
		this.shareProceeds=shareProceeds;
	}

	public String getShareProceeds(){
		return shareProceeds;
	}
	public void setEnabled(String enabled){
		this.enabled=enabled;
	}

	public String getEnabled(){
		return enabled;
	}
	public void setOrgId(String orgId){
		this.orgId=orgId;
	}

	public String getOrgId(){
		return orgId;
	}
	public void setCreateBy(String createBy){
		this.createBy=createBy;
	}

	public String getCreateBy(){
		return createBy;
	}
	public void setIsDelete(String isDelete){
		this.isDelete=isDelete;
	}

	public String getIsDelete(){
		return isDelete;
	}
	public void setCreateTime(Date createTime){
		this.createTime=createTime;
	}

	public Date getCreateTime(){
		return createTime;
	}
	public void setUpdateBy(String updateBy){
		this.updateBy=updateBy;
	}

	public String getUpdateBy(){
		return updateBy;
	}
	public void setUpdateTime(Date updateTime){
		this.updateTime=updateTime;
	}

	public Date getUpdateTime(){
		return updateTime;
	}
	public void setReserveFields1(String reserveFields1){
		this.reserveFields1=reserveFields1;
	}

	public String getReserveFields1(){
		return reserveFields1;
	}
	public void setReserveFields2(String reserveFields2){
		this.reserveFields2=reserveFields2;
	}

	public String getReserveFields2(){
		return reserveFields2;
	}
	public void setReserveFields3(String reserveFields3){
		this.reserveFields3=reserveFields3;
	}

	public String getReserveFields3(){
		return reserveFields3;
	}
	public void setReserveFields4(String reserveFields4){
		this.reserveFields4=reserveFields4;
	}

	public String getReserveFields4(){
		return reserveFields4;
	}
	public void setReserveFields5(String reserveFields5){
		this.reserveFields5=reserveFields5;
	}

	public String getReserveFields5(){
		return reserveFields5;
	}
	public void setReserveFields6(String reserveFields6){
		this.reserveFields6=reserveFields6;
	}

	public String getReserveFields6(){
		return reserveFields6;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("用户分享日志id  id= "+id+",");
	}
	if(null != logType && !"".equals(logType) ){
		sb.append("0影视宣发1版权投资  logType= "+logType+",");
	}
	if(null != shareUrl && !"".equals(shareUrl) ){
		sb.append("分享连接  shareUrl= "+shareUrl+",");
	}
	if(null != userId && !"".equals(userId) ){
		sb.append("分享文娱头条用户id  userId= "+userId+",");
	}
	if(null != linkId && !"".equals(linkId) ){
		sb.append("宣发或投资id  linkId= "+linkId+",");
	}
	if(null != isProceeds && !"".equals(isProceeds) ){
		sb.append("本次分享是否产生收益(0 否 1 是)  isProceeds= "+isProceeds+",");
	}
	if(null != shareProceeds && !"".equals(shareProceeds) ){
		sb.append("分享产生的收益金额  shareProceeds= "+shareProceeds+",");
	}
	if(null != enabled && !"".equals(enabled) ){
		sb.append("使用状态(0正常1禁用)  enabled= "+enabled+",");
	}
	if(null != orgId && !"".equals(orgId) ){
		sb.append("所属组织id  orgId= "+orgId+",");
	}
	if(null != createBy && !"".equals(createBy) ){
		sb.append("创建人id  createBy= "+createBy+",");
	}
	if(null != isDelete && !"".equals(isDelete) ){
		sb.append("0正常1删除  isDelete= "+isDelete+",");
	}
	if(null != createTime && !"".equals(createTime) ){
		sb.append("添加时间  createTime= "+createTime+",");
	}
	if(null != updateBy && !"".equals(updateBy) ){
		sb.append("修改人id  updateBy= "+updateBy+",");
	}
	if(null != updateTime && !"".equals(updateTime) ){
		sb.append("修改时间  updateTime= "+updateTime+",");
	}
	if(null != reserveFields1 && !"".equals(reserveFields1) ){
		sb.append("分享标题  reserveFields1= "+reserveFields1+",");
	}
	if(null != reserveFields2 && !"".equals(reserveFields2) ){
		sb.append("点击量  reserveFields2= "+reserveFields2+",");
	}
	if(null != reserveFields3 && !"".equals(reserveFields3) ){
		sb.append("预留字段3  reserveFields3= "+reserveFields3+",");
	}
	if(null != reserveFields4 && !"".equals(reserveFields4) ){
		sb.append("预留字段4  reserveFields4= "+reserveFields4+",");
	}
	if(null != reserveFields5 && !"".equals(reserveFields5) ){
		sb.append("预留字段5  reserveFields5= "+reserveFields5+",");
	}
	if(null != reserveFields6 && !"".equals(reserveFields6) ){
		sb.append("预留字段6  reserveFields6= "+reserveFields6+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
