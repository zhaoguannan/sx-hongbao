package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.*;

/**
* @TableName: wx_hongbao_info 
* @Package: com.sanxia.market.entity
* @Title:WxHongbaoInfo.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-01-24 11:11:02
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class WxHongbaoInfo extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :
	*/
	@ApiModelProperty(value = "" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields url :请求地址
	*/
	@ApiModelProperty(value = "请求地址" ,dataType = "String") 
	private String url;
	/**
	*@Fields mchName :商户名称
	*/
	@ApiModelProperty(value = "商户名称" ,dataType = "String") 
	private String mchName;
	/**
	*@Fields totalAmount :付款金额，单位分
	*/
	@ApiModelProperty(value = "付款金额，单位分" ,dataType = "String") 
	private String totalAmount;
	/**
	*@Fields wishing :红包祝福语
	*/
	@ApiModelProperty(value = "红包祝福语" ,dataType = "String") 
	private String wishing;
	/**
	*@Fields actName :活动名称
	*/
	@ApiModelProperty(value = "活动名称" ,dataType = "String") 
	private String actName;
	/**
	*@Fields remark :备注信息
	*/
	@ApiModelProperty(value = "备注信息" ,dataType = "String") 
	private String remark;
	/**
	*@Fields hongbaoType :红包类型
	*/
	@ApiModelProperty(value = "红包类型" ,dataType = "String") 
	private String hongbaoType;
	/**
	*@Fields beginTime :活动开始时间
	*/
	@ApiModelProperty(value = "活动开始时间" ,dataType = "Date") 
	private Date beginTime;
	/**
	*@Fields endTime :活动结束时间
	*/
	@ApiModelProperty(value = "活动结束时间" ,dataType = "Date") 
	private Date endTime;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setUrl(String url){
		this.url=url;
	}

	public String getUrl(){
		return url;
	}
	public void setMchName(String mchName){
		this.mchName=mchName;
	}

	public String getMchName(){
		return mchName;
	}
	public void setTotalAmount(String totalAmount){
		this.totalAmount=totalAmount;
	}

	public String getTotalAmount(){
		return totalAmount;
	}
	public void setWishing(String wishing){
		this.wishing=wishing;
	}

	public String getWishing(){
		return wishing;
	}
	public void setActName(String actName){
		this.actName=actName;
	}

	public String getActName(){
		return actName;
	}
	public void setRemark(String remark){
		this.remark=remark;
	}

	public String getRemark(){
		return remark;
	}
	public void setHongbaoType(String hongbaoType){
		this.hongbaoType=hongbaoType;
	}

	public String getHongbaoType(){
		return hongbaoType;
	}
	public void setBeginTime(Date beginTime){
		this.beginTime=beginTime;
	}

	public Date getBeginTime(){
		return beginTime;
	}
	public void setEndTime(Date endTime){
		this.endTime=endTime;
	}

	public Date getEndTime(){
		return endTime;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("  id= "+id+",");
	}
	if(null != url && !"".equals(url) ){
		sb.append("请求地址  url= "+url+",");
	}
	if(null != mchName && !"".equals(mchName) ){
		sb.append("商户名称  mchName= "+mchName+",");
	}
	if(null != totalAmount && !"".equals(totalAmount) ){
		sb.append("付款金额，单位分  totalAmount= "+totalAmount+",");
	}
	if(null != wishing && !"".equals(wishing) ){
		sb.append("红包祝福语  wishing= "+wishing+",");
	}
	if(null != actName && !"".equals(actName) ){
		sb.append("活动名称  actName= "+actName+",");
	}
	if(null != remark && !"".equals(remark) ){
		sb.append("备注信息  remark= "+remark+",");
	}
	if(null != hongbaoType && !"".equals(hongbaoType) ){
		sb.append("红包类型  hongbaoType= "+hongbaoType+",");
	}
	if(null != beginTime && !"".equals(beginTime) ){
		sb.append("活动开始时间  beginTime= "+beginTime+",");
	}
	if(null != endTime && !"".equals(endTime) ){
		sb.append("活动结束时间  endTime= "+endTime+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
