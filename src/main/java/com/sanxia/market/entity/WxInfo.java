package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
* @TableName: wx_info 
* @Package: com.sanxia.market.entity
* @Title:WxInfo.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-06-19 19:41:30
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class WxInfo extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :
	*/
	@ApiModelProperty(value = "" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields wxAppid :微信公众号appid
	*/
	@ApiModelProperty(value = "微信公众号appid" ,dataType = "String") 
	private String wxAppid;
	/**
	*@Fields wxMchId :微信商户号mch_id
	*/
	@ApiModelProperty(value = "微信商户号mch_id" ,dataType = "String") 
	private String wxMchId;
	/**
	*@Fields wxMchKey :注：key为商户平台设置的密钥key
	*/
	@ApiModelProperty(value = "注：key为商户平台设置的密钥key" ,dataType = "String") 
	private String wxMchKey;
	/**
	*@Fields mchName :平台商户名称
	*/
	@ApiModelProperty(value = "平台商户名称" ,dataType = "String") 
	private String mchName;
	/**
	*@Fields mchId :平台商户id
	*/
	@ApiModelProperty(value = "平台商户id" ,dataType = "Long") 
	private Long mchId;
	/**
	*@Fields wxAppSecret :微信公众号AppSecret
	*/
	@ApiModelProperty(value = "微信公众号AppSecret" ,dataType = "String") 
	private String wxAppSecret;
	/**
	*@Fields wxCertPath :微信证书路径（绝对路径）
	*/
	@ApiModelProperty(value = "微信证书路径（绝对路径）" ,dataType = "String") 
	private String wxCertPath;
	/**
	*@Fields accessToken :微信access_token
	*/
	@ApiModelProperty(value = "微信access_token" ,dataType = "String") 
	private String accessToken;
	/**
	*@Fields jsapiTicket :微信jsapi_ticket
	*/
	@ApiModelProperty(value = "微信jsapi_ticket" ,dataType = "String") 
	private String jsapiTicket;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setWxAppid(String wxAppid){
		this.wxAppid=wxAppid;
	}

	public String getWxAppid(){
		return wxAppid;
	}
	public void setWxMchId(String wxMchId){
		this.wxMchId=wxMchId;
	}

	public String getWxMchId(){
		return wxMchId;
	}
	public void setWxMchKey(String wxMchKey){
		this.wxMchKey=wxMchKey;
	}

	public String getWxMchKey(){
		return wxMchKey;
	}
	public void setMchName(String mchName){
		this.mchName=mchName;
	}

	public String getMchName(){
		return mchName;
	}
	public void setMchId(Long mchId){
		this.mchId=mchId;
	}

	public Long getMchId(){
		return mchId;
	}
	public void setWxAppSecret(String wxAppSecret){
		this.wxAppSecret=wxAppSecret;
	}

	public String getWxAppSecret(){
		return wxAppSecret;
	}
	public void setWxCertPath(String wxCertPath){
		this.wxCertPath=wxCertPath;
	}

	public String getWxCertPath(){
		return wxCertPath;
	}
	public void setAccessToken(String accessToken){
		this.accessToken=accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}
	public void setJsapiTicket(String jsapiTicket){
		this.jsapiTicket=jsapiTicket;
	}

	public String getJsapiTicket(){
		return jsapiTicket;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("  id= "+id+",");
	}
	if(null != wxAppid && !"".equals(wxAppid) ){
		sb.append("微信公众号appid  wxAppid= "+wxAppid+",");
	}
	if(null != wxMchId && !"".equals(wxMchId) ){
		sb.append("微信商户号mch_id  wxMchId= "+wxMchId+",");
	}
	if(null != wxMchKey && !"".equals(wxMchKey) ){
		sb.append("注：key为商户平台设置的密钥key  wxMchKey= "+wxMchKey+",");
	}
	if(null != mchName && !"".equals(mchName) ){
		sb.append("平台商户名称  mchName= "+mchName+",");
	}
	if(null != mchId && !"".equals(mchId) ){
		sb.append("平台商户id  mchId= "+mchId+",");
	}
	if(null != wxAppSecret && !"".equals(wxAppSecret) ){
		sb.append("微信公众号AppSecret  wxAppSecret= "+wxAppSecret+",");
	}
	if(null != wxCertPath && !"".equals(wxCertPath) ){
		sb.append("微信证书路径（绝对路径）  wxCertPath= "+wxCertPath+",");
	}
	if(null != accessToken && !"".equals(accessToken) ){
		sb.append("微信access_token  accessToken= "+accessToken+",");
	}
	if(null != jsapiTicket && !"".equals(jsapiTicket) ){
		sb.append("微信jsapi_ticket  jsapiTicket= "+jsapiTicket+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
