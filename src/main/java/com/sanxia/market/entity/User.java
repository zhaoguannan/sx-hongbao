package com.sanxia.market.entity;

import com.sanxia.common.core.entity.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.*;

/**
* @TableName: user 
* @Package: com.sanxia.market.entity
* @Title:User.java 
* @Description:  
* @author: 赵贯男
* @date: 2019-09-27 10:40:54
* @version V1.0    
* create by codeFactory
*/
@ApiModel("") 
public class User extends BaseEntity implements Serializable {

	  private static final long serialVersionUID = 1L;
	  
	/**
	*@Fields id :
	*/
	@ApiModelProperty(value = "" ,dataType = "Long") 
	private Long id;
	/**
	*@Fields userOpenid :用户openid
	*/
	@ApiModelProperty(value = "用户openid" ,dataType = "String") 
	private String userOpenid;
	/**
	*@Fields mechId :平台商户id
	*/
	@ApiModelProperty(value = "平台商户id" ,dataType = "String") 
	private String mechId;
	/**
	*@Fields userType :用户类型
	*/
	@ApiModelProperty(value = "用户类型" ,dataType = "String") 
	private String userType;
	/**
	*@Fields addTime :记录创建时间
	*/
	@ApiModelProperty(value = "记录创建时间" ,dataType = "Date") 
	private Date addTime;
	/**
	*@Fields userNickName :微信昵称
	*/
	@ApiModelProperty(value = "微信昵称" ,dataType = "String") 
	private String userNickName;
	/**
	*@Fields userPhoto :微信头像
	*/
	@ApiModelProperty(value = "微信头像" ,dataType = "String") 
	private String userPhoto;
	/**
	*@Fields parentUserId :父级id(没有父级默认0)
	*/
	@ApiModelProperty(value = "父级id(没有父级默认0)" ,dataType = "String") 
	private String parentUserId;
	/**
	*@Fields tureName :真实姓名
	*/
	@ApiModelProperty(value = "真实姓名" ,dataType = "String") 
	private String tureName;
	/**
	*@Fields userPhone :手机号
	*/
	@ApiModelProperty(value = "手机号" ,dataType = "String") 
	private String userPhone;
	/**
	*@Fields userAddress :用户详细地址
	*/
	@ApiModelProperty(value = "用户详细地址" ,dataType = "String") 
	private String userAddress;
	/**
	*@Fields userAnswerTime :用户剩余答题时间
	*/
	@ApiModelProperty(value = "用户剩余答题时间" ,dataType = "String") 
	private String userAnswerTime;
	/**
	*@Fields userAnswerScore :用户答题得分
	*/
	@ApiModelProperty(value = "用户答题得分" ,dataType = "String") 
	private String userAnswerScore;
	/**
	*@Fields submissionTime :提交时间
	*/
	@ApiModelProperty(value = "提交时间" ,dataType = "Date") 
	private Date submissionTime;
	/**
	*@Fields isSubmit :是否提交过(0未提交1已提交)
	*/
	@ApiModelProperty(value = "是否提交过(0未提交1已提交)" ,dataType = "String") 
	private String isSubmit;
	/**
	*@Fields reserveFields1 :预留字段1
	*/
	@ApiModelProperty(value = "预留字段1" ,dataType = "String") 
	private String reserveFields1;
	/**
	*@Fields reserveFields2 :预留字段2
	*/
	@ApiModelProperty(value = "预留字段2" ,dataType = "String") 
	private String reserveFields2;
	/**
	*@Fields reserveFields3 :预留字段3
	*/
	@ApiModelProperty(value = "预留字段3" ,dataType = "String") 
	private String reserveFields3;
	/**
	*@Fields reserveFields4 :预留字段4
	*/
	@ApiModelProperty(value = "预留字段4" ,dataType = "String") 
	private String reserveFields4;
	/**
	*@Fields reserveFields5 :预留字段5
	*/
	@ApiModelProperty(value = "预留字段5" ,dataType = "String") 
	private String reserveFields5;
	/**
	*@Fields reserveFields6 :预留字段6
	*/
	@ApiModelProperty(value = "预留字段6" ,dataType = "String") 
	private String reserveFields6;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return id;
	}
	public void setUserOpenid(String userOpenid){
		this.userOpenid=userOpenid;
	}

	public String getUserOpenid(){
		return userOpenid;
	}
	public void setMechId(String mechId){
		this.mechId=mechId;
	}

	public String getMechId(){
		return mechId;
	}
	public void setUserType(String userType){
		this.userType=userType;
	}

	public String getUserType(){
		return userType;
	}
	public void setAddTime(Date addTime){
		this.addTime=addTime;
	}

	public Date getAddTime(){
		return addTime;
	}
	public void setUserNickName(String userNickName){
		this.userNickName=userNickName;
	}

	public String getUserNickName(){
		return userNickName;
	}
	public void setUserPhoto(String userPhoto){
		this.userPhoto=userPhoto;
	}

	public String getUserPhoto(){
		return userPhoto;
	}
	public void setParentUserId(String parentUserId){
		this.parentUserId=parentUserId;
	}

	public String getParentUserId(){
		return parentUserId;
	}
	public void setTureName(String tureName){
		this.tureName=tureName;
	}

	public String getTureName(){
		return tureName;
	}
	public void setUserPhone(String userPhone){
		this.userPhone=userPhone;
	}

	public String getUserPhone(){
		return userPhone;
	}
	public void setUserAddress(String userAddress){
		this.userAddress=userAddress;
	}

	public String getUserAddress(){
		return userAddress;
	}
	public void setUserAnswerTime(String userAnswerTime){
		this.userAnswerTime=userAnswerTime;
	}

	public String getUserAnswerTime(){
		return userAnswerTime;
	}
	public void setUserAnswerScore(String userAnswerScore){
		this.userAnswerScore=userAnswerScore;
	}

	public String getUserAnswerScore(){
		return userAnswerScore;
	}
	public void setSubmissionTime(Date submissionTime){
		this.submissionTime=submissionTime;
	}

	public Date getSubmissionTime(){
		return submissionTime;
	}
	public void setIsSubmit(String isSubmit){
		this.isSubmit=isSubmit;
	}

	public String getIsSubmit(){
		return isSubmit;
	}
	public void setReserveFields1(String reserveFields1){
		this.reserveFields1=reserveFields1;
	}

	public String getReserveFields1(){
		return reserveFields1;
	}
	public void setReserveFields2(String reserveFields2){
		this.reserveFields2=reserveFields2;
	}

	public String getReserveFields2(){
		return reserveFields2;
	}
	public void setReserveFields3(String reserveFields3){
		this.reserveFields3=reserveFields3;
	}

	public String getReserveFields3(){
		return reserveFields3;
	}
	public void setReserveFields4(String reserveFields4){
		this.reserveFields4=reserveFields4;
	}

	public String getReserveFields4(){
		return reserveFields4;
	}
	public void setReserveFields5(String reserveFields5){
		this.reserveFields5=reserveFields5;
	}

	public String getReserveFields5(){
		return reserveFields5;
	}
	public void setReserveFields6(String reserveFields6){
		this.reserveFields6=reserveFields6;
	}

	public String getReserveFields6(){
		return reserveFields6;
	}

	public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	if(null != id && !"".equals(id) ){
		sb.append("  id= "+id+",");
	}
	if(null != userOpenid && !"".equals(userOpenid) ){
		sb.append("用户openid  userOpenid= "+userOpenid+",");
	}
	if(null != mechId && !"".equals(mechId) ){
		sb.append("平台商户id  mechId= "+mechId+",");
	}
	if(null != userType && !"".equals(userType) ){
		sb.append("用户类型  userType= "+userType+",");
	}
	if(null != addTime && !"".equals(addTime) ){
		sb.append("记录创建时间  addTime= "+addTime+",");
	}
	if(null != userNickName && !"".equals(userNickName) ){
		sb.append("微信昵称  userNickName= "+userNickName+",");
	}
	if(null != userPhoto && !"".equals(userPhoto) ){
		sb.append("微信头像  userPhoto= "+userPhoto+",");
	}
	if(null != parentUserId && !"".equals(parentUserId) ){
		sb.append("父级id(没有父级默认0)  parentUserId= "+parentUserId+",");
	}
	if(null != tureName && !"".equals(tureName) ){
		sb.append("真实姓名  tureName= "+tureName+",");
	}
	if(null != userPhone && !"".equals(userPhone) ){
		sb.append("手机号  userPhone= "+userPhone+",");
	}
	if(null != userAddress && !"".equals(userAddress) ){
		sb.append("用户详细地址  userAddress= "+userAddress+",");
	}
	if(null != userAnswerTime && !"".equals(userAnswerTime) ){
		sb.append("用户剩余答题时间  userAnswerTime= "+userAnswerTime+",");
	}
	if(null != userAnswerScore && !"".equals(userAnswerScore) ){
		sb.append("用户答题得分  userAnswerScore= "+userAnswerScore+",");
	}
	if(null != submissionTime && !"".equals(submissionTime) ){
		sb.append("提交时间  submissionTime= "+submissionTime+",");
	}
	if(null != isSubmit && !"".equals(isSubmit) ){
		sb.append("是否提交过(0未提交1已提交)  isSubmit= "+isSubmit+",");
	}
	if(null != reserveFields1 && !"".equals(reserveFields1) ){
		sb.append("预留字段1  reserveFields1= "+reserveFields1+",");
	}
	if(null != reserveFields2 && !"".equals(reserveFields2) ){
		sb.append("预留字段2  reserveFields2= "+reserveFields2+",");
	}
	if(null != reserveFields3 && !"".equals(reserveFields3) ){
		sb.append("预留字段3  reserveFields3= "+reserveFields3+",");
	}
	if(null != reserveFields4 && !"".equals(reserveFields4) ){
		sb.append("预留字段4  reserveFields4= "+reserveFields4+",");
	}
	if(null != reserveFields5 && !"".equals(reserveFields5) ){
		sb.append("预留字段5  reserveFields5= "+reserveFields5+",");
	}
	if(null != reserveFields6 && !"".equals(reserveFields6) ){
		sb.append("预留字段6  reserveFields6= "+reserveFields6+",");
	}
	sb.append("]");
	String toStr =sb.toString();
	return toStr.substring(0,toStr.indexOf(",]"))+"]";
	}

}
